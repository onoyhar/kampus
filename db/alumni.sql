-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 27, 2019 at 05:35 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alumni`
--

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `id` int(10) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `isi` text NOT NULL,
  `images` varchar(100) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id`, `judul`, `isi`, `images`, `tanggal`) VALUES
(3274, 'judul satu', '<p>Badan Kepegawaian Negara (BKN) Republik Indonesia memposting update SSCN 2018 beberpa jam lalu Selasa (2/10/2018).\r\n\r\n\r\nArtikel ini telah tayang di Tribunstyle.com dengan judul UPDATE CPNS 2018 - Universitas Terbuka Jadi Kampus Pelamar Tertinggi, Berikut Data Menarik Lainnya, http://style.tribunnews.com/2018/10/02/update-cpns-2018-universitas-terbuka-jadi-kampus-pelamar-tertinggi-berikut-data-menarik-lainnya.\r\nPenulis: Candra isriadhi	\r\nEditor: Dimas Setiawan Hutomo</p>\r\n', 'https://yt3.ggpht.com/a-/AAuE7mCKaRM4IkDbqY0G1v3-x9XAaTZD-8vjNwl0Sw=s900-mo-c-c0xffffffff-rj-k-no', '2019-01-10'),
(33424, 'test2', '<p>Badan Kepegawaian Negara (BKN) Republik Indonesia memposting update SSCN 2018 beberpa jam lalu Selasa (2/10/2018).\r\n', 'http://cdn2.tstatic.net/style/foto/bank/images/update-berita-cpns-2018_20180929_212554.jpg', '2019-01-24'),
(50096, 'dadawd', 'Badan Kepegawaian Negara (BKN) Republik Indonesia memposting update SSCN 2018 beberpa jam lalu Selasa (2/10/2018).\r\n\r\n\r\nArtikel ini telah tayang di Tribunstyle.com dengan judul UPDATE CPNS 2018 - Universitas Terbuka Jadi Kampus Pelamar Tertinggi, Berikut Data Menarik Lainnya, http://style.tribunnews.com/2018/10/02/update-cpns-2018-universitas-terbuka-jadi-kampus-pelamar-tertinggi-berikut-data-menarik-lainnya.\r\nPenulis: Candra isriadhi	\r\nEditor: Dimas Setiawan Hutomo', 'https://yt3.ggpht.com/a-/AAuE7mCKaRM4IkDbqY0G1v3-x9XAaTZD-8vjNwl0Sw=s900-mo-c-c0xffffffff-rj-k-no', '2019-01-18'),
(83942, 'test1', '<p>Badan Kepegawaian Negara (BKN) Republik Indonesia memposting update SSCN 2018 beberpa jam lalu Selasa (2/10/2018).\r\n', 'https://yt3.ggpht.com/a-/AAuE7mCKaRM4IkDbqY0G1v3-x9XAaTZD-8vjNwl0Sw=s900-mo-c-c0xffffffff-rj-k-no', '2019-01-11');

-- --------------------------------------------------------

--
-- Table structure for table `data_pribadi`
--

CREATE TABLE `data_pribadi` (
  `id` int(5) NOT NULL,
  `a1_npm` int(8) NOT NULL,
  `a2_namalengkap` varchar(200) NOT NULL,
  `a3_jeniskelamin` varchar(200) NOT NULL,
  `a4_tempatlahir` varchar(200) NOT NULL,
  `a5_tanggallahir` varchar(200) NOT NULL,
  `a6_nomorhp` varchar(200) NOT NULL,
  `a7_email` varchar(200) NOT NULL,
  `a8_alamatrumah` varchar(200) NOT NULL,
  `a9_alamatkantor` varchar(200) NOT NULL,
  `b1_tahunmasuk` varchar(100) NOT NULL,
  `b2_tahunlulus` varchar(100) NOT NULL,
  `b3_kodeprogramstudi` varchar(100) NOT NULL,
  `b4_setelahlulus` varchar(100) NOT NULL,
  `b5_namaperguruan` varchar(100) NOT NULL,
  `b6_tahunmasuk` varchar(100) NOT NULL,
  `b7_tahunlulus` varchar(100) NOT NULL,
  `b8_jenjangkuliah` varchar(100) NOT NULL,
  `b9_jurusan` varchar(100) NOT NULL,
  `c1_namatempatkerja` varchar(100) NOT NULL,
  `c2_jenisinstansi` varchar(100) NOT NULL,
  `c3_jabatan` varchar(100) NOT NULL,
  `c4_lamabekerja` varchar(100) NOT NULL,
  `c5_ratapendapatan` varchar(100) NOT NULL,
  `c6_berhubungan` varchar(100) NOT NULL,
  `c7_puaskerja` varchar(100) NOT NULL,
  `c8_pernahbekerja` varchar(100) NOT NULL,
  `c9_bergantikerja` varchar(100) NOT NULL,
  `c10_pindahkerja` varchar(100) NOT NULL,
  `d1_relevansipendidikankampus` varchar(100) NOT NULL,
  `d2_saran` varchar(200) NOT NULL,
  `d3_saatbarululus` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_pribadi`
--

INSERT INTO `data_pribadi` (`id`, `a1_npm`, `a2_namalengkap`, `a3_jeniskelamin`, `a4_tempatlahir`, `a5_tanggallahir`, `a6_nomorhp`, `a7_email`, `a8_alamatrumah`, `a9_alamatkantor`, `b1_tahunmasuk`, `b2_tahunlulus`, `b3_kodeprogramstudi`, `b4_setelahlulus`, `b5_namaperguruan`, `b6_tahunmasuk`, `b7_tahunlulus`, `b8_jenjangkuliah`, `b9_jurusan`, `c1_namatempatkerja`, `c2_jenisinstansi`, `c3_jabatan`, `c4_lamabekerja`, `c5_ratapendapatan`, `c6_berhubungan`, `c7_puaskerja`, `c8_pernahbekerja`, `c9_bergantikerja`, `c10_pindahkerja`, `d1_relevansipendidikankampus`, `d2_saran`, `d3_saatbarululus`) VALUES
(1, 10413064, 'Haryono', 'Laki-laki', 'jakarta', '15 april 1994', '08118671504', 'haryonoaryo@gmail.com', 'alamat rumah', 'alamat kantor', '2014', '2018', '23947', '234989', 'aaaaa', '1111', '1111', 'ffff', 'ffff', 'ffff', 'ffff', 'ffff', '1111', 'ffff', 'aaa', 'aaa', 'aaa', 'aaa', 'aaa', 'aaa', 'aaa', 'aaa'),
(79280, 4141, 'dede', 'perempuan', 'asdsa', 'asda', 'asda', 'dsada@asdad', 'asda', 'dasda', '', '2018', 'SI-57201', 'LanjutKuliah', '', '', '', 'Strata1', '', 'dasd', 'dasda', 'dasdsa', 'dasda', '< 5.000.000', 'Iya', 'Iya', 'Iya', '< 5.000.000', '', 'Iya', '', 'Sangat Mampu'),
(82344, 113123, 'asdsa', 'perempuan', 'asdsa', 'asda', 'asda', 'dsada@asdad', 'asda', 'dasda', '', '2018', 'SI-57201', 'LanjutKuliah', '', '', '', 'Strata1', '', 'dasd', 'dasda', 'dasdsa', 'dasda', '< 5.000.000', 'Iya', 'Iya', 'Iya', '< 5.000.000', '', 'Iya', '', 'Sangat Mampu');

-- --------------------------------------------------------

--
-- Table structure for table `lowongan`
--

CREATE TABLE `lowongan` (
  `id` int(10) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `namaperusahaan` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `tanggal` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lowongan`
--

INSERT INTO `lowongan` (`id`, `judul`, `namaperusahaan`, `isi`, `tanggal`) VALUES
(189, 'System Administrator', 'PT. Myindo Cyber Media', '<p>Explore opportunities to take your career to the next level. Whether you are a student, a graduate or an experienced professional, going to&nbsp;<strong>Indonizing The World</strong>&nbsp;with us at My<strong>indo</strong>.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Job Descriptions</strong></p>\r\n\r\n<ul>\r\n	<li>Membantu Head of Creative Department (HDC) dan divisi General Affair (GA) dalam hal pengadaan komputer dan software</li>\r\n	<li>Membantu HDC dan divisi GA dalam hal pengadaan sparepart dan alat-alat lain yang dibutuhkan</li>\r\n	<li>Memastikan peralatan terinstalasi dengan baik didalam maupun diluar kantor</li>\r\n	<li>Menginstal dan mengkonfigurasi sistem komputer dengan menggunakan software yang sah (legal)</li>\r\n	<li>Mendiagnosis kegagalan hardware dan software serta memecahkan masalah teknis dan aplikasi</li>\r\n	<li>Mengganti spareparts sesuai permintaan</li>\r\n	<li>Mengelola lisensi software yang digunakan perusahaan</li>\r\n	<li>Membantu proses penggunaan aplikasi baru ataupun penggantian aplikasi dengan versi baru</li>\r\n	<li>Membantu proses penggunaan software keamanan seperti virus, firewall,dll</li>\r\n	<li>Mengelola users account</li>\r\n	<li>Mengatasi masalah-masalah terkait password</li>\r\n	<li>Secara aktif bekerjasama dengan Network Analyst (NA) untuk memastikan, memonitor, dan meningkatkan keamanan penginstalan</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Job Requirements</strong></p>\r\n\r\n<ul>\r\n	<li>Pendidikan Min SMA/SMK</li>\r\n	<li>Usia Max. 28</li>\r\n	<li>Menguasai system operasi Linux dan Widows Server</li>\r\n	<li>Minimal 1 tahun pengalaman dalam merancang website</li>\r\n	<li>Mengerti Networking &amp; Troubleshooting</li>\r\n	<li>Mengerti IT Administrator</li>\r\n	<li>Mampu bekerjasama dalam team dan memiliki kemampuan analisa yang baik</li>\r\n	<li>Costumer service orientation</li>\r\n	<li>Memiliki keinginan yang tinggi untuk belajar</li>\r\n	<li>Mampu berkomunikasi bahasa Inggris dengan baik</li>\r\n	<li>Menguasai teknologi Proxying dan Address Translation</li>\r\n	<li>Mampu melakukan deployment layanan LAMP,Mail</li>\r\n	<li>Menguasai teknik netfiltering dan firewalling</li>\r\n	<li>Mampu menginstall berbagai OS</li>\r\n	<li>Menguasai dasar mail (zimbra dsb), NMS (Cacti, Nagios)</li>\r\n	<li>Menguasai install Apache &amp; MYSQL</li>\r\n	<li>Menguasai Linux terutama Redhat Base (Redhat/Cent OS)</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Send your CV and Portfolio to&nbsp;<strong>hrd@myindo.co.id</strong></p>\r\n', '2019-01-13 00:00:00'),
(7766, 'IT System Analyst', 'PT. Myindo Cyber Media', '<p>Explore opportunities to take your career to the next level. Whether you are a student, a graduate or an experienced professional, going to&nbsp;<strong>Indonizing The World</strong>&nbsp;with us at My<strong>indo</strong>.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Job Descriptions</strong></p>\r\n\r\n<ul>\r\n	<li>Work with team to obtain in-depth understanding of the user requirements or documentation requirements</li>\r\n	<li>Analyze the existing and potential cases</li>\r\n	<li>create and maintain the software architecture</li>\r\n	<li>Write easy to understand user interface text, online help and developer guides</li>\r\n	<li>Create tutorials to help user use applications</li>\r\n	<li>Handle the dinamic change request from user&nbsp;</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Job Requirements</strong></p>\r\n\r\n<ul>\r\n	<li>Min. S1 Science/information Technology or equivalent</li>\r\n	<li>Have a good knowledge about banking business processes is a plus</li>\r\n	<li>Strong analytical skill and good attention to detail</li>\r\n	<li>Good teamwork skill</li>\r\n	<li>Good communication skill</li>\r\n	<li>Good knowledge abaout Software Development Life Cycle</li>\r\n	<li>Able to use relevant tools</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Send your CV and Portfolio to&nbsp;<strong>hrd@myindo.co.id</strong></p>\r\n', '2018-12-24 00:00:00'),
(14690, 'IT System Analyst', 'PT. Myindo Cyber Media', '<p>Explore opportunities to take your career to the next level. Whether you are a student, a graduate or an experienced professional, going to&nbsp;<strong>Indonizing The World</strong>&nbsp;with us at My<strong>indo</strong>.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Job Descriptions</strong></p>\r\n\r\n<ul>\r\n	<li>Work with team to obtain in-depth understanding of the user requirements or documentation requirements</li>\r\n	<li>Analyze the existing and potential cases</li>\r\n	<li>create and maintain the software architecture</li>\r\n	<li>Write easy to understand user interface text, online help and developer guides</li>\r\n	<li>Create tutorials to help user use applications</li>\r\n	<li>Handle the dinamic change request from user&nbsp;</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Job Requirements</strong></p>\r\n\r\n<ul>\r\n	<li>Min. S1 Science/information Technology or equivalent</li>\r\n	<li>Have a good knowledge about banking business processes is a plus</li>\r\n	<li>Strong analytical skill and good attention to detail</li>\r\n	<li>Good teamwork skill</li>\r\n	<li>Good communication skill</li>\r\n	<li>Good knowledge abaout Software Development Life Cycle</li>\r\n	<li>Able to use relevant tools</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Send your CV and Portfolio to&nbsp;<strong>hrd@myindo.co.id</strong></p>\r\n', '2018-12-24 00:00:00'),
(14691, 'IT System Analyst', 'PT. Myindo Cyber Media', '<p>Explore opportunities to take your career to the next level. Whether you are a student, a graduate or an experienced professional, going to&nbsp;<strong>Indonizing The World</strong>&nbsp;with us at My<strong>indo</strong>.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Job Descriptions</strong></p>\r\n\r\n<ul>\r\n	<li>Work with team to obtain in-depth understanding of the user requirements or documentation requirements</li>\r\n	<li>Analyze the existing and potential cases</li>\r\n	<li>create and maintain the software architecture</li>\r\n	<li>Write easy to understand user interface text, online help and developer guides</li>\r\n	<li>Create tutorials to help user use applications</li>\r\n	<li>Handle the dinamic change request from user&nbsp;</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Job Requirements</strong></p>\r\n\r\n<ul>\r\n	<li>Min. S1 Science/information Technology or equivalent</li>\r\n	<li>Have a good knowledge about banking business processes is a plus</li>\r\n	<li>Strong analytical skill and good attention to detail</li>\r\n	<li>Good teamwork skill</li>\r\n	<li>Good communication skill</li>\r\n	<li>Good knowledge abaout Software Development Life Cycle</li>\r\n	<li>Able to use relevant tools</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Send your CV and Portfolio to&nbsp;<strong>hrd@myindo.co.id</strong></p>\r\n', '2018-12-24 00:00:00'),
(14692, 'IT System Analyst', 'PT. Myindo Cyber Media', '<p>Explore opportunities to take your career to the next level. Whether you are a student, a graduate or an experienced professional, going to&nbsp;<strong>Indonizing The World</strong>&nbsp;with us at My<strong>indo</strong>.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Job Descriptions</strong></p>\r\n\r\n<ul>\r\n	<li>Work with team to obtain in-depth understanding of the user requirements or documentation requirements</li>\r\n	<li>Analyze the existing and potential cases</li>\r\n	<li>create and maintain the software architecture</li>\r\n	<li>Write easy to understand user interface text, online help and developer guides</li>\r\n	<li>Create tutorials to help user use applications</li>\r\n	<li>Handle the dinamic change request from user&nbsp;</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Job Requirements</strong></p>\r\n\r\n<ul>\r\n	<li>Min. S1 Science/information Technology or equivalent</li>\r\n	<li>Have a good knowledge about banking business processes is a plus</li>\r\n	<li>Strong analytical skill and good attention to detail</li>\r\n	<li>Good teamwork skill</li>\r\n	<li>Good communication skill</li>\r\n	<li>Good knowledge abaout Software Development Life Cycle</li>\r\n	<li>Able to use relevant tools</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Send your CV and Portfolio to&nbsp;<strong>hrd@myindo.co.id</strong></p>\r\n', '2018-12-24 00:00:00'),
(14693, 'System Administrator', 'PT. Myindo Cyber Media', '<p>Explore opportunities to take your career to the next level. Whether you are a student, a graduate or an experienced professional, going to&nbsp;<strong>Indonizing The World</strong>&nbsp;with us at My<strong>indo</strong>.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Job Descriptions</strong></p>\r\n\r\n<ul>\r\n	<li>Membantu Head of Creative Department (HDC) dan divisi General Affair (GA) dalam hal pengadaan komputer dan software</li>\r\n	<li>Membantu HDC dan divisi GA dalam hal pengadaan sparepart dan alat-alat lain yang dibutuhkan</li>\r\n	<li>Memastikan peralatan terinstalasi dengan baik didalam maupun diluar kantor</li>\r\n	<li>Menginstal dan mengkonfigurasi sistem komputer dengan menggunakan software yang sah (legal)</li>\r\n	<li>Mendiagnosis kegagalan hardware dan software serta memecahkan masalah teknis dan aplikasi</li>\r\n	<li>Mengganti spareparts sesuai permintaan</li>\r\n	<li>Mengelola lisensi software yang digunakan perusahaan</li>\r\n	<li>Membantu proses penggunaan aplikasi baru ataupun penggantian aplikasi dengan versi baru</li>\r\n	<li>Membantu proses penggunaan software keamanan seperti virus, firewall,dll</li>\r\n	<li>Mengelola users account</li>\r\n	<li>Mengatasi masalah-masalah terkait password</li>\r\n	<li>Secara aktif bekerjasama dengan Network Analyst (NA) untuk memastikan, memonitor, dan meningkatkan keamanan penginstalan</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Job Requirements</strong></p>\r\n\r\n<ul>\r\n	<li>Pendidikan Min SMA/SMK</li>\r\n	<li>Usia Max. 28</li>\r\n	<li>Menguasai system operasi Linux dan Widows Server</li>\r\n	<li>Minimal 1 tahun pengalaman dalam merancang website</li>\r\n	<li>Mengerti Networking &amp; Troubleshooting</li>\r\n	<li>Mengerti IT Administrator</li>\r\n	<li>Mampu bekerjasama dalam team dan memiliki kemampuan analisa yang baik</li>\r\n	<li>Costumer service orientation</li>\r\n	<li>Memiliki keinginan yang tinggi untuk belajar</li>\r\n	<li>Mampu berkomunikasi bahasa Inggris dengan baik</li>\r\n	<li>Menguasai teknologi Proxying dan Address Translation</li>\r\n	<li>Mampu melakukan deployment layanan LAMP,Mail</li>\r\n	<li>Menguasai teknik netfiltering dan firewalling</li>\r\n	<li>Mampu menginstall berbagai OS</li>\r\n	<li>Menguasai dasar mail (zimbra dsb), NMS (Cacti, Nagios)</li>\r\n	<li>Menguasai install Apache &amp; MYSQL</li>\r\n	<li>Menguasai Linux terutama Redhat Base (Redhat/Cent OS)</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Send your CV and Portfolio to&nbsp;<strong>hrd@myindo.co.id</strong></p>\r\n', '2019-01-13 00:00:00'),
(14694, 'System Administrator', 'PT. Myindo Cyber Media', '<p>Explore opportunities to take your career to the next level. Whether you are a student, a graduate or an experienced professional, going to&nbsp;<strong>Indonizing The World</strong>&nbsp;with us at My<strong>indo</strong>.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Job Descriptions</strong></p>\r\n\r\n<ul>\r\n	<li>Membantu Head of Creative Department (HDC) dan divisi General Affair (GA) dalam hal pengadaan komputer dan software</li>\r\n	<li>Membantu HDC dan divisi GA dalam hal pengadaan sparepart dan alat-alat lain yang dibutuhkan</li>\r\n	<li>Memastikan peralatan terinstalasi dengan baik didalam maupun diluar kantor</li>\r\n	<li>Menginstal dan mengkonfigurasi sistem komputer dengan menggunakan software yang sah (legal)</li>\r\n	<li>Mendiagnosis kegagalan hardware dan software serta memecahkan masalah teknis dan aplikasi</li>\r\n	<li>Mengganti spareparts sesuai permintaan</li>\r\n	<li>Mengelola lisensi software yang digunakan perusahaan</li>\r\n	<li>Membantu proses penggunaan aplikasi baru ataupun penggantian aplikasi dengan versi baru</li>\r\n	<li>Membantu proses penggunaan software keamanan seperti virus, firewall,dll</li>\r\n	<li>Mengelola users account</li>\r\n	<li>Mengatasi masalah-masalah terkait password</li>\r\n	<li>Secara aktif bekerjasama dengan Network Analyst (NA) untuk memastikan, memonitor, dan meningkatkan keamanan penginstalan</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Job Requirements</strong></p>\r\n\r\n<ul>\r\n	<li>Pendidikan Min SMA/SMK</li>\r\n	<li>Usia Max. 28</li>\r\n	<li>Menguasai system operasi Linux dan Widows Server</li>\r\n	<li>Minimal 1 tahun pengalaman dalam merancang website</li>\r\n	<li>Mengerti Networking &amp; Troubleshooting</li>\r\n	<li>Mengerti IT Administrator</li>\r\n	<li>Mampu bekerjasama dalam team dan memiliki kemampuan analisa yang baik</li>\r\n	<li>Costumer service orientation</li>\r\n	<li>Memiliki keinginan yang tinggi untuk belajar</li>\r\n	<li>Mampu berkomunikasi bahasa Inggris dengan baik</li>\r\n	<li>Menguasai teknologi Proxying dan Address Translation</li>\r\n	<li>Mampu melakukan deployment layanan LAMP,Mail</li>\r\n	<li>Menguasai teknik netfiltering dan firewalling</li>\r\n	<li>Mampu menginstall berbagai OS</li>\r\n	<li>Menguasai dasar mail (zimbra dsb), NMS (Cacti, Nagios)</li>\r\n	<li>Menguasai install Apache &amp; MYSQL</li>\r\n	<li>Menguasai Linux terutama Redhat Base (Redhat/Cent OS)</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Send your CV and Portfolio to&nbsp;<strong>hrd@myindo.co.id</strong></p>\r\n', '2019-01-13 00:00:00'),
(14695, 'System Administrator', 'PT. Myindo Cyber Media', '<p>Explore opportunities to take your career to the next level. Whether you are a student, a graduate or an experienced professional, going to&nbsp;<strong>Indonizing The World</strong>&nbsp;with us at My<strong>indo</strong>.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Job Descriptions</strong></p>\r\n\r\n<ul>\r\n	<li>Membantu Head of Creative Department (HDC) dan divisi General Affair (GA) dalam hal pengadaan komputer dan software</li>\r\n	<li>Membantu HDC dan divisi GA dalam hal pengadaan sparepart dan alat-alat lain yang dibutuhkan</li>\r\n	<li>Memastikan peralatan terinstalasi dengan baik didalam maupun diluar kantor</li>\r\n	<li>Menginstal dan mengkonfigurasi sistem komputer dengan menggunakan software yang sah (legal)</li>\r\n	<li>Mendiagnosis kegagalan hardware dan software serta memecahkan masalah teknis dan aplikasi</li>\r\n	<li>Mengganti spareparts sesuai permintaan</li>\r\n	<li>Mengelola lisensi software yang digunakan perusahaan</li>\r\n	<li>Membantu proses penggunaan aplikasi baru ataupun penggantian aplikasi dengan versi baru</li>\r\n	<li>Membantu proses penggunaan software keamanan seperti virus, firewall,dll</li>\r\n	<li>Mengelola users account</li>\r\n	<li>Mengatasi masalah-masalah terkait password</li>\r\n	<li>Secara aktif bekerjasama dengan Network Analyst (NA) untuk memastikan, memonitor, dan meningkatkan keamanan penginstalan</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Job Requirements</strong></p>\r\n\r\n<ul>\r\n	<li>Pendidikan Min SMA/SMK</li>\r\n	<li>Usia Max. 28</li>\r\n	<li>Menguasai system operasi Linux dan Widows Server</li>\r\n	<li>Minimal 1 tahun pengalaman dalam merancang website</li>\r\n	<li>Mengerti Networking &amp; Troubleshooting</li>\r\n	<li>Mengerti IT Administrator</li>\r\n	<li>Mampu bekerjasama dalam team dan memiliki kemampuan analisa yang baik</li>\r\n	<li>Costumer service orientation</li>\r\n	<li>Memiliki keinginan yang tinggi untuk belajar</li>\r\n	<li>Mampu berkomunikasi bahasa Inggris dengan baik</li>\r\n	<li>Menguasai teknologi Proxying dan Address Translation</li>\r\n	<li>Mampu melakukan deployment layanan LAMP,Mail</li>\r\n	<li>Menguasai teknik netfiltering dan firewalling</li>\r\n	<li>Mampu menginstall berbagai OS</li>\r\n	<li>Menguasai dasar mail (zimbra dsb), NMS (Cacti, Nagios)</li>\r\n	<li>Menguasai install Apache &amp; MYSQL</li>\r\n	<li>Menguasai Linux terutama Redhat Base (Redhat/Cent OS)</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Send your CV and Portfolio to&nbsp;<strong>hrd@myindo.co.id</strong></p>\r\n', '2019-01-13 00:00:00'),
(14696, 'Java Programmer', 'PT. Haryono IT Solution', '<p>Explore opportunities to take your career to the next level. Whether you are a student, a graduate or an experienced professional, going to&nbsp;<strong>Indonizing The World</strong>&nbsp;with us at My<strong>indo</strong>.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Job Descriptions</strong></p>\r\n\r\n<ul>\r\n	<li>Membantu Head of Creative Department (HDC) dan divisi General Affair (GA) dalam hal pengadaan komputer dan software</li>\r\n	<li>Membantu HDC dan divisi GA dalam hal pengadaan sparepart dan alat-alat lain yang dibutuhkan</li>\r\n	<li>Memastikan peralatan terinstalasi dengan baik didalam maupun diluar kantor</li>\r\n	<li>Menginstal dan mengkonfigurasi sistem komputer dengan menggunakan software yang sah (legal)</li>\r\n	<li>Mendiagnosis kegagalan hardware dan software serta memecahkan masalah teknis dan aplikasi</li>\r\n	<li>Mengganti spareparts sesuai permintaan</li>\r\n	<li>Mengelola lisensi software yang digunakan perusahaan</li>\r\n	<li>Membantu proses penggunaan aplikasi baru ataupun penggantian aplikasi dengan versi baru</li>\r\n	<li>Membantu proses penggunaan software keamanan seperti virus, firewall,dll</li>\r\n	<li>Mengelola users account</li>\r\n	<li>Mengatasi masalah-masalah terkait password</li>\r\n	<li>Secara aktif bekerjasama dengan Network Analyst (NA) untuk memastikan, memonitor, dan meningkatkan keamanan penginstalan</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Job Requirements</strong></p>\r\n\r\n<ul>\r\n	<li>Pendidikan Min SMA/SMK</li>\r\n	<li>Usia Max. 28</li>\r\n	<li>Menguasai system operasi Linux dan Widows Server</li>\r\n	<li>Minimal 1 tahun pengalaman dalam merancang website</li>\r\n	<li>Mengerti Networking &amp; Troubleshooting</li>\r\n	<li>Mengerti IT Administrator</li>\r\n	<li>Mampu bekerjasama dalam team dan memiliki kemampuan analisa yang baik</li>\r\n	<li>Costumer service orientation</li>\r\n	<li>Memiliki keinginan yang tinggi untuk belajar</li>\r\n	<li>Mampu berkomunikasi bahasa Inggris dengan baik</li>\r\n	<li>Menguasai teknologi Proxying dan Address Translation</li>\r\n	<li>Mampu melakukan deployment layanan LAMP,Mail</li>\r\n	<li>Menguasai teknik netfiltering dan firewalling</li>\r\n	<li>Mampu menginstall berbagai OS</li>\r\n	<li>Menguasai dasar mail (zimbra dsb), NMS (Cacti, Nagios)</li>\r\n	<li>Menguasai install Apache &amp; MYSQL</li>\r\n	<li>Menguasai Linux terutama Redhat Base (Redhat/Cent OS)</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Send your CV and Portfolio to&nbsp;<strong>hrd@myindo.co.id</strong></p>\r\n', '2019-01-13 00:00:00'),
(14697, 'PHP Progammer', 'PT. Haryono IT Solution', '<p>Explore opportunities to take your career to the next level. Whether you are a student, a graduate or an experienced professional, going to&nbsp;<strong>Indonizing The World</strong>&nbsp;with us at My<strong>indo</strong>.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Job Descriptions</strong></p>\r\n\r\n<ul>\r\n	<li>Membantu Head of Creative Department (HDC) dan divisi General Affair (GA) dalam hal pengadaan komputer dan software</li>\r\n	<li>Membantu HDC dan divisi GA dalam hal pengadaan sparepart dan alat-alat lain yang dibutuhkan</li>\r\n	<li>Memastikan peralatan terinstalasi dengan baik didalam maupun diluar kantor</li>\r\n	<li>Menginstal dan mengkonfigurasi sistem komputer dengan menggunakan software yang sah (legal)</li>\r\n	<li>Mendiagnosis kegagalan hardware dan software serta memecahkan masalah teknis dan aplikasi</li>\r\n	<li>Mengganti spareparts sesuai permintaan</li>\r\n	<li>Mengelola lisensi software yang digunakan perusahaan</li>\r\n	<li>Membantu proses penggunaan aplikasi baru ataupun penggantian aplikasi dengan versi baru</li>\r\n	<li>Membantu proses penggunaan software keamanan seperti virus, firewall,dll</li>\r\n	<li>Mengelola users account</li>\r\n	<li>Mengatasi masalah-masalah terkait password</li>\r\n	<li>Secara aktif bekerjasama dengan Network Analyst (NA) untuk memastikan, memonitor, dan meningkatkan keamanan penginstalan</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Job Requirements</strong></p>\r\n\r\n<ul>\r\n	<li>Pendidikan Min SMA/SMK</li>\r\n	<li>Usia Max. 28</li>\r\n	<li>Menguasai system operasi Linux dan Widows Server</li>\r\n	<li>Minimal 1 tahun pengalaman dalam merancang website</li>\r\n	<li>Mengerti Networking &amp; Troubleshooting</li>\r\n	<li>Mengerti IT Administrator</li>\r\n	<li>Mampu bekerjasama dalam team dan memiliki kemampuan analisa yang baik</li>\r\n	<li>Costumer service orientation</li>\r\n	<li>Memiliki keinginan yang tinggi untuk belajar</li>\r\n	<li>Mampu berkomunikasi bahasa Inggris dengan baik</li>\r\n	<li>Menguasai teknologi Proxying dan Address Translation</li>\r\n	<li>Mampu melakukan deployment layanan LAMP,Mail</li>\r\n	<li>Menguasai teknik netfiltering dan firewalling</li>\r\n	<li>Mampu menginstall berbagai OS</li>\r\n	<li>Menguasai dasar mail (zimbra dsb), NMS (Cacti, Nagios)</li>\r\n	<li>Menguasai install Apache &amp; MYSQL</li>\r\n	<li>Menguasai Linux terutama Redhat Base (Redhat/Cent OS)</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Send your CV and Portfolio to&nbsp;<strong>hrd@myindo.co.id</strong></p>\r\n', '2019-01-13 00:00:00'),
(14698, 'Quality Control', 'PT. Haryono IT Solution', '<p>Explore opportunities to take your career to the next level. Whether you are a student, a graduate or an experienced professional, going to&nbsp;<strong>Indonizing The World</strong>&nbsp;with us at My<strong>indo</strong>.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Job Descriptions</strong></p>\r\n\r\n<ul>\r\n	<li>Membantu Head of Creative Department (HDC) dan divisi General Affair (GA) dalam hal pengadaan komputer dan software</li>\r\n	<li>Membantu HDC dan divisi GA dalam hal pengadaan sparepart dan alat-alat lain yang dibutuhkan</li>\r\n	<li>Memastikan peralatan terinstalasi dengan baik didalam maupun diluar kantor</li>\r\n	<li>Menginstal dan mengkonfigurasi sistem komputer dengan menggunakan software yang sah (legal)</li>\r\n	<li>Mendiagnosis kegagalan hardware dan software serta memecahkan masalah teknis dan aplikasi</li>\r\n	<li>Mengganti spareparts sesuai permintaan</li>\r\n	<li>Mengelola lisensi software yang digunakan perusahaan</li>\r\n	<li>Membantu proses penggunaan aplikasi baru ataupun penggantian aplikasi dengan versi baru</li>\r\n	<li>Membantu proses penggunaan software keamanan seperti virus, firewall,dll</li>\r\n	<li>Mengelola users account</li>\r\n	<li>Mengatasi masalah-masalah terkait password</li>\r\n	<li>Secara aktif bekerjasama dengan Network Analyst (NA) untuk memastikan, memonitor, dan meningkatkan keamanan penginstalan</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Job Requirements</strong></p>\r\n\r\n<ul>\r\n	<li>Pendidikan Min SMA/SMK</li>\r\n	<li>Usia Max. 28</li>\r\n	<li>Menguasai system operasi Linux dan Widows Server</li>\r\n	<li>Minimal 1 tahun pengalaman dalam merancang website</li>\r\n	<li>Mengerti Networking &amp; Troubleshooting</li>\r\n	<li>Mengerti IT Administrator</li>\r\n	<li>Mampu bekerjasama dalam team dan memiliki kemampuan analisa yang baik</li>\r\n	<li>Costumer service orientation</li>\r\n	<li>Memiliki keinginan yang tinggi untuk belajar</li>\r\n	<li>Mampu berkomunikasi bahasa Inggris dengan baik</li>\r\n	<li>Menguasai teknologi Proxying dan Address Translation</li>\r\n	<li>Mampu melakukan deployment layanan LAMP,Mail</li>\r\n	<li>Menguasai teknik netfiltering dan firewalling</li>\r\n	<li>Mampu menginstall berbagai OS</li>\r\n	<li>Menguasai dasar mail (zimbra dsb), NMS (Cacti, Nagios)</li>\r\n	<li>Menguasai install Apache &amp; MYSQL</li>\r\n	<li>Menguasai Linux terutama Redhat Base (Redhat/Cent OS)</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Send your CV and Portfolio to&nbsp;<strong>hrd@myindo.co.id</strong></p>\r\n', '2019-01-13 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_about`
--

CREATE TABLE `tb_about` (
  `id` int(5) NOT NULL,
  `judul` varchar(32) NOT NULL,
  `isi` text NOT NULL,
  `logo` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_about`
--

INSERT INTO `tb_about` (`id`, `judul`, `isi`, `logo`) VALUES
(1, 'Tentang Kami', 'institusi pendidikan tinggi diharapkan banyak berperan baik sebagai sumber ilmu pengetahuan maupun penghasil tenaga kerja terdidik. Proses pembelajaran yang dilaksanakan seharusnya mampu membekali kompetensi lulusan sesuai kebutuhan dunia kerja, sekaligus kompetensi untuk dapat membuka peluang kerja. STMIK JAKARTA STI&K merupakan sebuah institusi yang menyadari akan pentingnya tanggungjawab dan pengabdian terhadap masyarakat terutama civitas akademikanya. Salah satu tanggung jawabnya adalah menangani pengembangan karir bagi alumni dan pembekalan kompetensi lulusan. Kecepatan dalam memperoleh informasi lowongan pekerjaan dan kompetensi yang dimiliki oleh lulusan diharapkan mampu memperpendek masa tunggu lulusan dalam mendapatkan pekerjaan. Untuk itulah dibutuhkan suatu biro layanan yang mampu memenuhi kepentingan dan kebutuhan yang dimaksud. Maka dibentuklah pusat layanan karir bagi mahasiswa dan alumni yaitu STMIK JAKARTA STI&K career centre.', 'fa fa-users wc-icon'),
(2, 'Visi dan Misi', '<h1>Visi</h1>\r\n<p>Menjadi pusat pengembangan karir yang mampu menghasilkan lulusan yang unggul dibidangnya dan dapat mengikuti perkembangan ilmu pengetahuan dan teknologi terhadap dunia kerja di era global.\r\n</p>\r\n<h1>Misi</h1>\r\n<p>Sebagai mediator antara mahasiswa dan Alumni STMIK JAKARTA STI&K dengan perusahaan/institusi. Mengelola penyebaran Informasi lowongan ketenagakerjaan untuk memenuhi kebutuhan mahasiswa dan alumni. Menyelenggarakan kegiatan yang berhubungan dengan pengembangan karir dengan memberikan seminar karir yang bertujuan untuk menghasilkan Alumni yang siap kerja dan selalu dicari oleh perusahaan/institusi. Meningkatkan keterserapan lulusan STMIK JAKARTA STI&K dalam dunia kerja dengan masa tunggu untuk memperoleh pekerjaan yang relatif pendek. Membangun jejaring kerja sama dengan dunia kerja.</p>', 'fa fa-users wc-icon'),
(3, 'Maksud dan Tujuan', '<h1>Maksud </h1>\r\n<ol>\r\n<li>Mempersiapkan mahasiswa dalam menghadapi persaingan dunia kerja</li>\r\n<li>Mengelola dan memberdayakan peluang karir</li>\r\n<li>Menjalin kerjasama yang berkesinambungan dengan dunia industri</li>\r\n<li>Melakukan pendampingan kepada mahasiswa/alumni dan dunia kerja dalam perekrutan SDM</li>\r\n<li>Menyampaikan informasi lowongan pekerjaan bagi mahaiswa dan alumni</li>\r\n<li>emenuhi kebutuhan stakeholder untuk publikasi lowongan kerja</li>\r\n</ol>\r\n\r\n\r\n<h1>Tujuan</h1>\r\nSebagai fasilitator dan mediator dalam hal persiapan, penempatan dan pengembangan karier bagi mahasiswa dan alumni STMIK JAKARTA STI&K.', 'fa fa-users wc-icon');

-- --------------------------------------------------------

--
-- Table structure for table `tb_detail_profile`
--

CREATE TABLE `tb_detail_profile` (
  `id` int(5) NOT NULL,
  `judul` varchar(32) NOT NULL,
  `isi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_detail_profile`
--

INSERT INTO `tb_detail_profile` (`id`, `judul`, `isi`) VALUES
(1, 'Tentang Kami', 'Institusi pendidikan tinggi diharapkan banyak berperan baik sebagai sumber ilmu pengetahuan maupun penghasil tenaga kerja terdidik. Proses pembelajaran yang dilaksanakan seharusnya mampu membekali kompetensi lulusan sesuai kebutuhan dunia kerja, sekaligus kompetensi untuk dapat membuka peluang kerja. STMIK JAKARTA STI&K merupakan sebuah institusi yang menyadari akan pentingnya tanggungjawab dan pengabdian terhadap masyarakat terutama civitas akademikanya. Salah satu tanggung jawabnya adalah menangani pengembangan karir bagi alumni dan pembekalan kompetensi lulusan. Kecepatan dalam memperoleh informasi lowongan pekerjaan dan kompetensi yang dimiliki oleh lulusan diharapkan mampu memperpendek masa tunggu lulusan dalam mendapatkan pekerjaan. Untuk itulah dibutuhkan suatu biro layanan yang mampu memenuhi kepentingan dan kebutuhan yang dimaksud. Maka dibentuklah pusat layanan karir bagi mahasiswa dan alumni yaitu STMIK JAKARTA STI&K career centre.'),
(2, 'Visi dan Misi', '<h1>Visi</h1>\r\n<p>Menjadi pusat pengembangan karir yang mampu menghasilkan lulusan yang unggul dibidangnya dan dapat mengikuti perkembangan ilmu pengetahuan dan teknologi terhadap dunia kerja di era global.</p>\r\n<h1>Misi</h1>\r\n<p>\r\nSebagai mediator antara mahasiswa dan Alumni STMIK JAKARTA STI&K dengan perusahaan/institusi. Mengelola penyebaran Informasi lowongan ketenagakerjaan untuk memenuhi kebutuhan mahasiswa dan alumni. Menyelenggarakan kegiatan yang berhubungan dengan pengembangan karir dengan memberikan seminar karir yang bertujuan untuk menghasilkan Alumni yang siap kerja dan selalu dicari oleh perusahaan/institusi. Meningkatkan keterserapan lulusan STMIK JAKARTA STI&K dalam dunia kerja dengan masa tunggu untuk memperoleh pekerjaan yang relatif pendek. Membangun jejaring kerja sama dengan dunia kerja.</p>'),
(3, 'Maksud dan Tujuan', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userid` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `user_password` varchar(50) NOT NULL,
  `user_level` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userid`, `name`, `user_name`, `user_password`, `user_level`) VALUES
(121, 'Administrator', 'admin', 'admin', 'staff');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_pribadi`
--
ALTER TABLE `data_pribadi`
  ADD PRIMARY KEY (`id`,`a1_npm`);

--
-- Indexes for table `lowongan`
--
ALTER TABLE `lowongan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_about`
--
ALTER TABLE `tb_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_detail_profile`
--
ALTER TABLE `tb_detail_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83943;

--
-- AUTO_INCREMENT for table `data_pribadi`
--
ALTER TABLE `data_pribadi`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82345;

--
-- AUTO_INCREMENT for table `lowongan`
--
ALTER TABLE `lowongan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14699;

--
-- AUTO_INCREMENT for table `tb_about`
--
ALTER TABLE `tb_about`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_detail_profile`
--
ALTER TABLE `tb_detail_profile`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
