	<!-- news -->
	<div class="news-section py-5">
		<div class="container py-xl-5 py-lg-3">
			<h3 class="title text-capitalize font-weight-light text-dark text-center mb-5">our lastest
				<span class="font-weight-bold">news</span>
			</h3>
			<div class="row news-grids-w3l pt-md-4">
				<?php 
                  include("db_connect.php");
                  $show_berita = mysqli_query($connect,"SELECT * FROM berita LIMIT 3");
                  while($row = mysqli_fetch_array($show_berita)) {
                ?>
				
				<div class="col-md-4 news-grid">
					<a href="views/berita/detailberita.php">
						<img style="height: 350px; width: 350px;" src="<?php echo $row['images']; ?>" class="img-fluid" alt="" />
					</a>
					<div class="news-text" style="overflow: hidden;height: 30% ;width: 100%;  ">
						<div class="news-events-agile event-colo3 py-2 px-3" style="background-color: #54adf6;">
							<h5 class="float-left">
								<a href="views/berita/detailberita.php" class="text-white"></a><?php echo $row['tanggal']; ?>
							</h5>
							<div class="post-img float-right">
								<ul>
									<li>
										<a href="#">
											<i class="far fa-comments text-white"></i>
										</a>
									</li>
									<li class="mx-3">
										<a href="#">
											<i class="far fa-heart text-white"></i>
										</a>
									</li>
									<li>
										<a href="#">
											<i class="fas fa-share text-white"></i>
										</a>
									</li>
								</ul>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="detail-bottom">
							<h6 class="my-3">
								<a href="views/berita/detailberita.php" class="text-dark">
									<?php echo $row['judul	']; ?>
								</a>
							</h6>
							<p><?php echo $row['isi']; ?></p>
						</div>
					</div>
				</div>
				<?php 
                    }
                ?>
			</div>
			<div class="button-lihat" style="position: relative;top: 50%;left: 55%;margin-left: -220px;" >
				<a href="views/berita/kumpulanberita.php" class="btn btn-primary" style="color: white;">Lihat Banyak Lowongan</a>
			</div>
				<!-- <a href="views/berita/detailberita.php" class="btn btn-primary" style="color: white;">Lihat Banyak Lowongan</a> -->
		</div>
	</div>
	<!-- //news 