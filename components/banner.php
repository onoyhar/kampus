	<!-- banner -->
	<div class="banner-agile">
		<ul class="slider">
			<li class="active">
				<div class="banner-w3ls-1">
				</div>
			</li>
			<li>
				<div class="banner-w3ls-2">
				</div>
			</li>
			<li>
				<div class="banner-w3ls-3">
				</div>
			</li>
			<li>
				<div class="banner-w3ls-4">
				</div>
			</li>
			<li class="prev">
				<div class="banner-w3ls-5">
				</div>
			</li>
		</ul>
		<ul class="pager">
			<li data-index="0" class="active"></li>
			<li data-index="1"></li>
			<li data-index="2"></li>
			<li data-index="3"></li>
			<li data-index="4"></li>
		</ul>
		<div class="banner-text-posi-w3ls">
			<div class="banner-text-whtree">
				<h3 class="text-capitalize text-white p-4">your bright future
					<b>is our mission!</b>
				</h3>
				<p class="px-4 py-3 text-dark">Become top-of-the-flight specialist after graduating!</p>
				<a href="../views/about/index.php" class="button-agiles text-capitalize text-white mt-sm-5 mt-4">read more</a>
			</div>
		</div>

		<!-- navigation -->
		<div class="navigation-w3ls">
			<nav class="navbar navbar-expand-lg navbar-light bg-light sticky-nav">
				<button class="navbar-toggler mx-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
				 aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse text-center" id="navbarSupportedContent">
					<ul class="navbar-nav justify-content-center">
						<li class="nav-item active">
							<a class="nav-link text-white" href="../index.php">Home
								<span class="sr-only">(current)</span>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" href="../views/about/index.php">About Us</a>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle text-white" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Tracer Study
							</a>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="../views/kuisioner/datakuisioner.php">Data Alumni</a>
								<a class="dropdown-item" href="../views/kuisioner/kuisioner.php">Kuisioner</a>
							</div>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" href="../views/berita/kumpulanberita.php">Berita</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white" href="../views/lowongan/listlowongan.php">Lowongan Lerja</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
		<!-- //navigation -->
	</div>
	<!-- //banner -->
