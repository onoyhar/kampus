<?php
  include('cekstaff.php');
  include("../db_connect.php");
  include("function.php");
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Staff STIMIK Dharma Negara Bandung | Dashboard</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="../core/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="../core/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="../core/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="../core/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../core/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="../core/bower_components/morris.js/morris.css">
  <link rel="stylesheet" href="../core/bower_components/jvectormap/jquery-jvectormap.css">
  <link rel="stylesheet" href="../core/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <link rel="stylesheet" href="../core/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="../core/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-yellow-light sidebar-collapse sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <a href="" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b></b>STIMIK</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Staff </b>STIMIK</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">

      </div>
    </nav>
  </header>
