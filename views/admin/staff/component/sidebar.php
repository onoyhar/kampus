<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Circle-icons-profile.svg/1000px-Circle-icons-profile.svg.png" class="img-circle" alt="User Image">
      </div>
      <?php
        $userid = $_SESSION['userid'];
        $show_user = mysqli_query($connect,"SELECT * FROM users where userid='$userid' ");
          while($row = mysqli_fetch_array($show_user)) {
       ?>
      <div class="pull-left info">
        <p><?php echo $row['name']?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
      <?php
        }
       ?>
    </div>

    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li><a href="index.php"><i class="fa fa-dashboard"></i> <span>Beranda</span></a></li>
      <li><a href="alumni.php"><i class="fa fa-dashboard"></i> <span>Data Alumni</span></a></li>
      <li><a href="berita.php"><i class="fa fa-edit"></i> <span>Berita</span></a></li>
      <li><a href="lowongan.php"><i class="fa fa-edit"></i> <span>Lowongan Kerja</span></a></li>

      <li class="header"></li>

      <li><a href="change-password.php"><i class="fa fa-key text-yellow"></i> <span>Change Password</span></a></li>
      <li><a href="logout.php"><i class="fa fa-sign-out text-red"></i> <span>Logout</span></a></li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
