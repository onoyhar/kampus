<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <!-- Untuk menampilkan Pengumuman -->

    <section class="content-header">

    </section>
    <section class="content">
      <div class="row">
              <?php
                  date_default_timezone_set("Asia/Jakarta");
                  require_once '../db_connect.php';
                  ?>
          <div class="col-md-12">
            <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Data Detail:</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div  style="word-wrap: break-word;">
                  <?php
                    //ambil id yg ada di url
                      if($_GET['id']){
                        $id = $_GET['id'];
                        //buat query
                        $sql = "SELECT * FROM data_pribadi WHERE id = {$id}";

                        //jalankan query
                        $result = $connect->query($sql);


                        //masukkan hasil query ke dalam variabel data
                        $data = $result->fetch_assoc();

                        //putuhkan koneksi
                        $connect->close();
                      ?>

                      <p>
                        <!-- <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                          Data Pribadi
                        </button> -->
                        <a ><b>Data Pribadi</b></a>
                        <a class="pull-right" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample"> hide/show</a>
                      </p>
                      <div class="collapse in" id="collapseExample">
                        <div class="card card-body">
<!--                           <p>Data Pribadi</p>
 -->                            <table class="table table-striped table-bordered">
                                <thead>
                                  <tr>
                                    <th style="width: 40%">Kuisioner</th>
                                    <th>Deskipsi</th>
                                  </tr>
                                </thead>
                                    <tbody>
                                    <tr>
                                      <td>NPM</td>
                                      <td><?php echo $data['id']; ?></td>
                                    </tr>
                                    <tr>
                                      <td>Nama Lengkap</td>
                                      <td><?php echo $data['a2_namalengkap']; ?></td>
                                    </tr>
                                    <tr>
                                      <td>Jenis Kelamin</td>
                                      <td><?php echo $data['a3_jeniskelamin']; ?></td>
                                    </tr>
                                    <tr>
                                      <td>Tempat Lahir</td>
                                      <td><?php echo $data['a4_tempatlahir']; ?></td>
                                    </tr>
                                    <tr>
                                      <td>Tanggal Lahir</td>
                                      <td><?php echo $data['a5_tanggallahir']; ?></td>
                                    </tr>
                                    <tr>
                                      <td>Nomor HP</td>
                                      <td><?php echo $data['a6_nomorhp']; ?></td>
                                    </tr>
                                    <tr>
                                      <td>Alamat Rumah</td>
                                      <td><?php echo $data['a8_alamatrumah']; ?></td>
                                    </tr>
                                    <tr>
                                      <td>Alamat Kantor</td>
                                      <td><?php echo $data['a9_alamatkantor']; ?></td>
                                    </tr>
                              </tbody>
                            </table>
                        </div>
                      </div>
                

                      <p>
                        <!-- <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapsedatapendidikan" aria-expanded="false" aria-controls="collapsedatapendidikan">
                          Data Pendidikan
                        </button> -->
                        <a ><b>Data Pendidikan</b></a>
                        <a class="pull-right" data-toggle="collapse" href="#collapsedatapendidikan" role="button" aria-expanded="false" aria-controls="collapsedatapendidikan"> hide/show</a>
                      </p>
                      <div class="collapse in" id="collapsedatapendidikan">
                        <div class="card card-body">
                          <table class="table table-striped table-bordered">
                              <thead>
                                <tr>
                                  <th style="width: 40%">Kuisioner</th>
                                  <th>Deskipsi</th>
                                </tr>
                              </thead>
                                  <tbody>
                                  <tr>
                                    <td>Tahun Masuk</td>
                                    <td><?php echo $data['b1_tahunmasuk']; ?></td>
                                  </tr>
                                  <tr>
                                    <td>Tahun Lulus</td>
                                    <td><?php echo $data['b2_tahunlulus']; ?></td>
                                  </tr>
                                  <tr>
                                    <td>kode Program Studi</td>
                                    <td><?php echo $data['b3_kodeprogramstudi']; ?></td>
                                  </tr>
                                  <tr>
                                    <td>Setelah Lulus apakah anda melanjutkan kuliah lagi ?</td>
                                    <td><?php echo $data['b4_setelahlulus']; ?></td>
                                  </tr>
                                  <tr>
                                    <td>Nama Perguruan Tinggi</td>
                                    <td><?php echo $data['b5_namaperguruan']; ?></td>
                                  </tr>
                                  <tr>
                                    <td>Tahun Masuk</td>
                                    <td><?php echo $data['b6_tahunmasuk']; ?></td>
                                  </tr>
                                  <tr>
                                    <td>Tahun Lulus</td>
                                    <td><?php echo $data['b7_tahunlulus']; ?></td>
                                  </tr>
                                  <tr>
                                    <td>Alamat Kantor</td>
                                    <td><?php echo $data['a9_alamatkantor']; ?></td>
                                  </tr>
                                  <tr>
                                    <td>Jenjang Kuliah</td>
                                    <td><?php echo $data['b8_jenjangkuliah']; ?></td>
                                  </tr>
                                  <tr>
                                    <td>Jurusan</td>
                                    <td><?php echo $data['b9_jurusan']; ?></td>
                                  </tr>
                            </tbody>
                          </table>
                         </div>
                      </div>

                      <p>
                        <!-- <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapsedatapekerjaan" aria-expanded="false" aria-controls="collapsedatapekerjaan">
                          Data Pekerjaan
                        </button> -->
                        <a><b>Data Pekerjaan</b></a>
                        <a class="pull-right" data-toggle="collapse" href="#collapsedatapekerjaan" role="button" aria-expanded="false" aria-controls="collapsedatapekerjaan"> hide/show</a>
                      </p>
                      <div class="collapse in" id="collapsedatapekerjaan">
                        <div class="card card-body">
                            <table class="table table-striped table-bordered">
                                <thead>
                                  <tr>
                                    <th style="width: 40%">Kuisioner</th>
                                    <th>Deskipsi</th>
                                  </tr>
                                </thead>
                                    <tbody>
                                    <tr>
                                      <td>Nama Tempat Bekerja Sekarang</td>
                                      <td><?php echo $data['c1_namatempatkerja']; ?></td>
                                    </tr>
                                    <tr>
                                      <td>Jenis instansi Saat Ini</td>
                                      <td><?php echo $data['c2_jenisinstansi']; ?></td>
                                    </tr>
                                    <tr>
                                      <td>Jabatan/Posisi Di Pekerjaan Sekarang</td>
                                      <td><?php echo $data['c3_jabatan']; ?></td>
                                    </tr>
                                    <tr>
                                      <td>Setelah Lulus apakah anda melanjutkan kuliah lagi ?</td>
                                      <td><?php echo $data['b4_setelahlulus']; ?></td>
                                    </tr>
                                    <tr>
                                      <td>Bulan dan Tahun Bekerja dipekerjaan Sekarang</td>
                                      <td><?php echo $data['c4_lamabekerja']; ?></td>
                                    </tr>
                                    <tr>
                                      <td>Berapa Rata-rata Seluruh Pendapatan Anda Sekarang</td>
                                      <td><?php echo $data['c5_ratapendapatan']; ?></td>
                                    </tr>
                                    <tr>
                                      <td>Apakah Pekerjaan Anda Saat Ini Berhubungan dengan Bidang ilmu yang Anda Pelajari</td>
                                      <td><?php echo $data['c6_berhubungan']; ?></td>
                                    </tr>
                                    <tr>
                                      <td>Apakah Puas Dengan pekerjaan Sekarang?</td>
                                      <td><?php echo $data['c7_puaskerja']; ?></td>
                                    </tr>
                                    <tr>
                                      <td>Sebelumnya, Apakah Anda pernah bekerja Ditempat lain?</td>
                                      <td><?php echo $data['c8_pernahbekerja']; ?></td>
                                    </tr>
                                    <tr>
                                      <td>Berapa Rata-rata Seluruh Pendapatan Anda Sekarang</td>
                                      <td><?php echo $data['c9_bergantikerja']; ?></td>
                                    </tr>
                                    <tr>
                                      <td>Sudah Berapa kali anda Berganti pekerjaan? (*ktik Jumlah)</td>
                                      <td><?php echo $data['c10_pindahkerja']; ?></td>
                                    </tr>
                                    <tr>
                                      <td>Apakah Anda Masih Ingin Berpindah Bekerja?</td>
                                      <td><?php echo $data['c11_mungkinpindahkerja']; ?></td>
                                    </tr>
                              </tbody>
                            </table>
                         </div>
                      </div>

                      <p>
                        <!-- <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapserelevansi" aria-expanded="false" aria-controls="collapserelevansi">
                          Relevansi Pendidikan dengan Pekerjaa
                        </button> -->
                        <a ><b>Relevansi Pendidikan dengan Pekerjaan</b></a>
                        <a class="pull-right" data-toggle="collapse" href="#collapserelevansi" role="button" aria-expanded="false" aria-controls="collapserelevansi"> hide/show</a>
                      </p>
                      <div class="collapse in" id="collapserelevansi">
                        <div class="card card-body">
                          <table class="table table-striped table-bordered">
                              <thead>
                                <tr>
                                  <th style="width: 40%">Kuisioner</th>
                                  <th>Deskipsi</th>
                                </tr>
                              </thead>
                                  <tbody>
                                  <tr>
                                    <td>Apakah pendidikan yang Anda Ddapat Di STMIk Relevan dengan Pekerjaan Saudara?</td>
                                    <td><?php echo $data['d1_relevansipendidikankampus']; ?></td>
                                  </tr>
                                  <tr>
                                    <td>Dari Pengalaman Anda bekerja, Apa Sarab praktis Anda Untuk Pendidikan DI STMIK Dalam rangak Menigkatkan Kesesuainan Antara Pendidikan dengak Lapangan Pekerjaan?</td>
                                    <td><?php echo $data['d1_relevansipendidikankampus']; ?></td>
                                  </tr>
                                  <tr>
                                    <td>Saat baru Lulus, Apkah Anda Mampu bersaing dengan lulusan Perguruan tinggi lain?</td>
                                    <td><?php echo $data['d3_saatbarululus']; ?></td>
                                  </tr>
                              </tbody>
                          </table>
                         </div>
                      </div>

                  <?php } ?>
 
              </div>
              <a href="#" class="btn btn-default pull-right"><span class="glyphicon glyphicon-print"></span> Cetak </a>
            </div>
            <div class="box-footer">
              <a href="alumni.php" class="btn btn-default">Kembali </a>
            </div>
          </div>
          </div>
      </div>

    </section>
  </div>
  <?php
  include("component/footer.php");
   ?>
