<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <!-- Untuk menampilkan Pengumuman -->

    <section class="content-header">

    </section>
    <section class="content">
      <div class="row">
          		<?php
                  date_default_timezone_set("Asia/Jakarta");
                  require_once '../db_connect.php';
                  ?>
          <div class="col-md-12">
            <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Data Detail:</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            	<div  style="word-wrap: break-word;">
		            	<?php
		            		//ambil id yg ada di url
			                if($_GET['id']){
			                  $id = $_GET['id'];
			                  //buat query
			                  $sql = "SELECT * FROM lowongan WHERE id = {$id}";

			                  //jalankan query
			                  $result = $connect->query($sql);


			                  //masukkan hasil query ke dalam variabel data
			                  $data = $result->fetch_assoc();

			                  //putuhkan koneksi
			                  $connect->close();
		                  ?>


			            	<p><b>ID: <br/></b><?php echo $data['id']; ?> </p> 
			            	<p><b>Tanggal dibuat: <br/></b><?php echo $data['tanggal']; ?> </p>             	
			            	<p><b>Judul: <br/></b><?php echo $data['judul']; ?> </p>
		 
			            	<p><b>ISI Lowongan: </b><?php echo $data['isi']; ?> </p>

		            	<?php } ?>
            	</div>
            </div>
            <div class="box-footer">
            	<a href="lowongan.php" class="btn btn-default">Kembali </a>
            </div>
          </div>
          </div>
      </div>

    </section>
  </div>
  <?php
  include("component/footer.php");
   ?>
