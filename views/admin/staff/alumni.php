<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <!-- Untuk menampilkan Pengumuman -->

    <section class="content-header">

    </section>
    <section class="content">
      <div class="row">

          <div class="col-md-12">
            <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Data Alumni:</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>NO</th>
                  <th>ID</th>
                  <th>Nama</th>
                  <th>NPM</th>
                  <th>Program Studi</th>
                  <th>Tahun Lulus</th>
                  <th>Action</th>
                </tr>
                </thead>
                <?php 
                  include("../db_connect.php");
                  $show_berita = mysqli_query($connect,"SELECT * FROM  data_pribadi");
                  $no=0;
                  while($row = mysqli_fetch_array($show_berita)) {
                     $no++;
                ?>
                <tbody>
                <tr>
                  <td><?php echo $no;?></td>
                  <td><?php echo $row['id']; ?></td>
                  <td><?php echo $row['a2_namalengkap']; ?></td>
                  <td><?php echo $row['a1_npm']; ?></td>
                  <td><?php echo $row['b3_kodeprogramstudi']; ?></td>
                  <td><?php echo $row['b2_tahunlulus']; ?></td>
                  <td><a href="detailmasteralumni.php?id=<?php echo $row['id'];?>" class="btn btn-xs btn-primary">detail </a>
                      <a href="delete.php?id=<?php echo $row['id']; ?>" class="btn btn-xs btn-danger">delete </a>
                  </td>
                </tr>

                </tbody>
                <?php } ?>
              </table>
            </div>
          </div>
          </div>
      </div>

    </section>
  </div>
  <?php
  include("component/footer.php");
   ?>
