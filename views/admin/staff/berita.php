<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper"> 
    <!-- Untuk menampilkan Pengumuman -->

    <section class="content-header">

    </section>
    <section class="content">
      <div class="row">
          <div class="col-md-5 col-xs-8">
            <div class="box box-warning">
              <div class="card-form">
                <?php
                   require_once '../db_connect.php';
                 
                  if(!empty($_GET['id'])){
                 
                  $id = $_GET['id'];

                        //buat query
                   $query_mysql = mysqli_query($connect,"SELECT * FROM berita WHERE id='$id'")or die(mysql_error());
                    $nomor = 1;
                    while($data = mysqli_fetch_array($query_mysql)){

                ?>
                <form role="form" method="POST" action="berita.php">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1"> Judul : </label>
                      <input type="hidden" name="id" value="<?php echo $data['id'] ?>">
                      <input type="text" class="form-control" name="judul" placeholder="Masukkan Judul Berita" value="<?php echo $data['judul']?>" required>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Berita :</label>
                      <div class="box-body pad">
                        <textarea id="editor1" name="isi" rows="10" cols="80" style="visibility: hidden; display: none;"  /><?php echo $data['isi']?>

                        </textarea>
                      </div>
                    </div>
                  </div>
                  <div class="box-footer">
                    <button type="submit" name="edit" class="btn btn-primary">Update</button>

                  </div>

                </form>
              <?php }
            }else{ ?>
              <form role="form" method="POST" action="berita.php">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1"> Judul : </label>
                      <input type="text" class="form-control" name="judul" placeholder="Masukkan Judul Berita" >
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Berita :</label>
                      <div class="box-body pad">
                        <textarea id="editor1" name="isi" rows="10" cols="80" style="visibility: hidden; display: none;"  />

                        </textarea>
                      </div>
                    </div>
                  </div>
                  <div class="box-footer">
                    <button type="submit" name="save" class="btn btn-primary">Submit</button>

                  </div>

                </form>
                <?php
            }
               ?>
                <?php
                  date_default_timezone_set("Asia/Jakarta");
                  require_once '../db_connect.php';
                  if(isset($_POST['save'])) {
                      $id = rand(100,99999);
                      $judul = $_POST['judul'];
                      $isi = $_POST['isi'];
                      $date = date('Y-m-d');
                      $sql = "INSERT INTO berita (id, judul, isi, tanggal)
                      VALUES ('$id','$judul','$isi','$date')
                      ";
                      if ($connect-> query($sql) === TRUE ) {
                      echo "
                      <script type='text/javascript'>
                          alert('Berita ".$judul." Berhasil ditambah');
                          window.location = 'berita.php';
                      </script>";
                      } else {
                      echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                      }
                      $connect->close();
                      }else if(isset($_POST['edit'])){
                          $id = $_POST['id'];
                          $judul = $_POST['judul'];
                          $isi = $_POST['isi'];
                          $date = date('Y-m-d');
                          $sql = "UPDATE berita set  judul='$judul', isi='$isi', tanggal='$date'
                          WHERE id='$id'
                          ";
                         // print_r($sql);die;
                          if ($connect-> query($sql) === TRUE ) {
                          echo "
                          <script type='text/javascript'>
                              alert('Berita ".$judul." Berhasil Update');
                              window.location = 'berita.php';
                          </script>";
                          } else {
                          echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                          }
                          $connect->close();
                      }
                  ?>
              </div>
            </div>
          </div>


          <div class="col-md-7">
            <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Data Berita:</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabel-data" class="table table-bordered">
                <thead>
                <tr>
                  <th>ID Berita</th>
                  <th>Judul</th>
                  <th>Action</th>
                </tr>
                </thead>
                 <tbody>
                <?php
                  $show = mysqli_query($connect,"SELECT * FROM berita");
                  while($row = mysqli_fetch_array($show)) {
                ?>
               
                <tr>
                  <td><?php echo $row['id']; ?></td>
                  <td><?php echo $row['judul']; ?></td>
                  <td>
                    <a href="berita-view.php?id=<?php echo $row['id'];?>" class="btn btn-xs btn-primary"> View</a>
                    <a href="?id=<?php echo $row['id'];?>" class="btn btn-xs btn-default"> edit</a>
                    <a href="delete-berita.php?id=<?php echo $row['id']; ?>" class="btn btn-xs btn-danger">delete</a>
                  </td>


                </tr>
                   <?php } ?>
                </tbody>
             
              </table>
            </div>
          </div>
          </div>
      </div>

    </section>
  </div>
  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <!-- <script src="../../assets/js/dataTables.bootstrap.js"></script> -->

    <script type="text/javascript">
        $(function() {
            $('#tabel-data').dataTable();
        });
    </script>

  <?php
 // include("component/footer.php");
   ?>
