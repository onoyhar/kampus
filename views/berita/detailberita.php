<?php 
include("../components/header.php");
?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="../../index.php">Home</a>
		</li>
		<li class="breadcrumb-item active" aria-current="page">Berita</li>
	</ol>
</nav>
<!-- single -->
<div class="single-w3l py-5">
	<div class="container py-xl-5 py-lg-3">
		<h3 class="title text-capitalize font-weight-light text-dark text-center mb-5">Judul
			<span class="font-weight-bold">Berita</span>
		</h3>
		<div class="row inner_sec_info pt-md-4">
			<!-- left side -->
			<?php 
			include("../../db_connect.php");
			$id = $_GET['id'];
			$show_berita = mysqli_query($connect,"SELECT * FROM berita where id = $id");
			while($row = mysqli_fetch_array($show_berita)) {
				?>
				<div class="col-lg-8 single-left">
					<div class="single-left1">
						<img src="<?php echo $row['images']; ?>" alt=" " class="img-fluid" />
						<h6 class="blog-first text-dark text-center my-4">
							<i class="far fa-user mr-2"></i><?php echo $row['judul']; ?>
						</h6>
						<ul class="blog_list my-3 text-center">
							<li><?php echo $row['tanggal']; ?></li>
							<li class="mx-3">
								<a href="#">
									<i class="far fa-heart mr-1"></i>
								22</a>
							</li>
							<li>
								<a href="#">
									<i class="far fa-comments mr-1"></i>
								16</a>
							</li>
						</ul>
						<h5 class="card-title">
							<!-- <a href="single.html" class="text-dark">Sed ut perspiciatis unde omnis iste natus error sit facilisis erat posuere erat</a> -->
						</h5>
						<p><?php echo $row['isi']; ?></p>
					</div>
				</div>
			<?php } ?>  




			<!-- right side -->
			<div class="col-lg-4 event-right mt-lg-0 mt-sm-5 mt-4" style="">
				<div class="event-right1" >
					<div class="search1">
						<form class="form-inline" action="#" method="post">
							<input class="form-control rounded-0 mr-sm-2" type="search" placeholder="Search Here" aria-label="Search" required>
							<button class="btn bg-dark text-white rounded-0 mt-3" type="submit">Search</button>
						</form>
					</div>
					<div class="categories my-4 p-4 border">
						
						<h3 class="blog-title text-dark">Berita Lainnya</h3>
						<ul>
							<li class="mt-3">
								<?php 
								include("../../db_connect.php");
					                  // $id = $_GET['id'];
								$show_berita = mysqli_query($connect,"SELECT * FROM berita");
								while($row = mysqli_fetch_array($show_berita)) {
									?>
									<i class="fas fa-check mr-2"></i>
									<a href="detailberita.php?id=<?php echo $row['id']; ?>"></i><?php echo $row['judul']; ?></a><br><br>	
								<?php }?>
							</li>
						</ul>
					</div>
				</div>



				<div class="posts p-4 border">
					<h3 class="blog-title text-dark">Lowongan Kerja</h3>
					<?php 
					include("../../db_connect.php");
			                  // $id = $_GET['id'];
					$show_lowongan = mysqli_query($connect,"SELECT * FROM lowongan limit 5" );
					while($row = mysqli_fetch_array($show_lowongan)) {
						?>
						<div class="posts-grids">
							<div class="row posts-grid mt-4">
								<div class="col-lg-4 col-md-3 col-4 posts-grid-left pr-0">
									<a href="../lowongan/detaillowongan.php?id=<?php echo $row['id']; ?>">
										<img src="<?php echo $row['images']; ?>" alt=" " class="img-fluid" />
									</a>
								</div>
								<div class="col-lg-8 col-md-7 col-8 posts-grid-right mt-lg-0 mt-md-5 mt-sm-4">
									<h4>
										<a href="../lowongan/detaillowongan.php?id=<?php echo $row['id']; ?>" class="text-dark"><?php echo $row['judul']; ?></a>
									</h4>
									<ul class="wthree_blog_events_list mt-2">
										<li class="mr-2 text-dark">
											<i class="fa fa-calendar mr-2" aria-hidden="true"></i><?php echo $row['tanggal']; ?></li>
											<li>
												<i class="fa fa-user" aria-hidden="true"></i>
												<a href="#" class="text-dark ml-2">Admin</a>
											</li>
										</ul>
									</div>
								</div>
								
							</div>
						<?php } ?>
					</div>
				</div>
				<!-- //right side -->
			</div>
		</div>
	</div>
	<!-- //blog -->

<?php
	include('footer.php');
?>