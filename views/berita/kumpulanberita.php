<?php 
include("../components/header.php");
?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="../../index.php">Home</a>
		</li>
		<li class="breadcrumb-item active" aria-current="page">List Berita</li>
	</ol>
</nav>

<!-- blog -->
	<div class="blog-w3l py-5">
		<div class="container py-xl-5 py-lg-3">
			<h3 class="title text-capitalize font-weight-light text-dark text-center mb-5">Kumpulan
				<span class="font-weight-bold">Berita</span>
			</h3>
			<div class="row blog-content pt-md-4">
				<!-- left side -->
				
				<div class="col-lg-8 blog_section">
				<?php
					// Include / load file koneksi.php
					include("../../db_connect.php");
					
					// Cek apakah terdapat data page pada URL
					$page = (isset($_GET['page']))? $_GET['page'] : 1;
					
					$limit = 2; // Jumlah data per halamannya
					
					// Untuk menentukan dari data ke berapa yang akan ditampilkan pada tabel yang ada di database
					$limit_start = ($page - 1) * $limit;
					
					// Buat query untuk menampilkan data siswa sesuai limit yang ditentukan
					$sql = mysqli_query($connect, "SELECT * FROM berita LIMIT ".$limit_start.",".$limit);
					
					$no = $limit_start + 1; // Untuk penomoran tabel
					while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
				?>
					<div class="card">
						<img class="card-img-top" src="<?php echo $data['images']; ?>" alt="">
						<div class="card-body text-center">
							<h6 class="blog-first text-dark">
								<i class="far fa-user mr-2"></i><?php echo $data['judul']; ?>
							</h6>
							<ul class="blog_list my-3">
								<li><?php echo $data['tanggal']; ?></li>
								<li class="mx-3">
									<a href="#">
										<i class="far fa-heart mr-1"></i>
									30</a>
								</li>
								<li>
									<a href="#">
										<i class="far fa-comments mr-1"></i>
									18</a>
								</li>
							</ul>
							<h5 class="card-title">
								<a href="#" class="text-dark"><?php echo $data['judul']; ?></a>
							</h5>
							<p class="card-text"><?php echo substr($data['isi'], 0, 242); ?></p>
							<a href="detailberita.php?id=<?php echo $data['id']; ?>" class="btn btn-primary blog-button mt-3">Read More</a>
						</div>
					</div><br>
					<?php 
				}
				?>

				<!--
			-- Buat Paginationnya
			-- Dengan bootstrap, kita jadi dimudahkan untuk membuat tombol-tombol pagination dengan design yang bagus tentunya
			-->
			<nav aria-label="Page navigation example">
			<ul class="pagination mt-5">
				<!-- LINK FIRST AND PREV -->
				<?php
				if($page == 1){ // Jika page adalah page ke 1, maka disable link PREV
				?>
					<li class="page-item disabled"><a class="page-link href="#">First</a></li>
					<li class="page-item disabled"><a class="page-link href="#">&laquo;</a></li>
				<?php
				}else{ // Jika page bukan page ke 1
					$link_prev = ($page > 1)? $page - 1 : 1;
				?>
					<li><a class="page-link" href="kumpulanberita.php?page=1">First</a></li>
					<li><a class="page-link" href="kumpulanberita.php?page=<?php echo $link_prev; ?>">&laquo;</a></li>
				<?php
				}
				?>
				
				<!-- LINK NUMBER -->
				<?php
				// Buat query untuk menghitung semua jumlah data
				$sql2 = mysqli_query($connect, "SELECT COUNT(*) AS jumlah FROM berita");
				$get_jumlah = mysqli_fetch_array($sql2);
				
				$jumlah_page = ceil($get_jumlah['jumlah'] / $limit); // Hitung jumlah halamannya
				$jumlah_number = 2; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
				$start_number = ($page > $jumlah_number)? $page - $jumlah_number : 1; // Untuk awal link number
				$end_number = ($page < ($jumlah_page - $jumlah_number))? $page + $jumlah_number : $jumlah_page; // Untuk akhir link number
				
				for($i = $start_number; $i <= $end_number; $i++){
					$link_active = ($page == $i)? ' class="active"' : '';
				?>
					<li<?php echo $link_active; ?>><a class="page-link" href="kumpulanberita.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
				<?php
				}
				?>
				
				<!-- LINK NEXT AND LAST -->
				<?php
				// Jika page sama dengan jumlah page, maka disable link NEXT nya
				// Artinya page tersebut adalah page terakhir 
				if($page == $jumlah_page){ // Jika page terakhir
				?>
					<li class="page-item disabled"><a class="page-link" href="#">&raquo;</a></li>
					<li class="page-item disabled"><a class="page-link" href="#">Last</a></li>
				<?php
				}else{ // Jika Bukan page terakhir
					$link_next = ($page < $jumlah_page)? $page + 1 : $jumlah_page;
				?>
					<li class="page-item"><a class="page-link" href="kumpulanberita.php?page=<?php echo $link_next; ?>">&raquo;</a></li>
					<li class="page-item"><a class="page-link" href="kumpulanberita.php?page=<?php echo $jumlah_page; ?>">Last</a></li>
				<?php
				}
				?>
			</ul>
			</nav>
			</div>

			<!-- right side -->
			<!-- right side -->
			<div class="col-lg-4 event-right mt-lg-0 mt-sm-5 mt-4" style="">
				<div class="event-right1" >
					<div class="search1">
						<form class="form-inline" action="#" method="post">
							<input class="form-control rounded-0 mr-sm-2" type="search" placeholder="Search Here" aria-label="Search" required>
							<button class="btn bg-dark text-white rounded-0 mt-3" type="submit">Search</button>
						</form>
					</div>
					<div class="categories my-4 p-4 border">
						
						<h3 class="blog-title text-dark">Berita Lainnya</h3>
						<ul>
							<li class="mt-3">
								<?php 
								include("../../db_connect.php");
					                  // $id = $_GET['id'];
								$show_berita = mysqli_query($connect,"SELECT * FROM berita");
								while($row = mysqli_fetch_array($show_berita)) {
									?>
									<i class="fas fa-check mr-2"></i>
									<a href="detailberita.php?id=<?php echo $row['id']; ?>"></i><?php echo $row['judul']; ?></a><br><br>
								<?php }?>
							</li>
						</ul>
					</div>
				</div>


				<div class="posts p-4 border">
					<h3 class="blog-title text-dark">Lowongan Kerja</h3>
					<?php 
					include("../../db_connect.php");
			                  // $id = $_GET['id'];
					$show_lowongan = mysqli_query($connect,"SELECT * FROM lowongan limit 5" );
					while($row = mysqli_fetch_array($show_lowongan)) {
						?>
						<div class="posts-grids">
							<div class="row posts-grid mt-4">
								<div class="col-lg-4 col-md-3 col-4 posts-grid-left pr-0">
									<a href="lowonganberita.php?id=<?php echo $row['id']; ?>">
										<img src="<?php echo $row['images']; ?>" alt=" " class="img-fluid" />
									</a>
								</div>
								<div class="col-lg-8 col-md-7 col-8 posts-grid-right mt-lg-0 mt-md-5 mt-sm-4">
									<h4>
										<a href="lowonganberita.php?id=<?php echo $row['id']; ?>" class="text-dark"><?php echo $row['judul']; ?></a>
									</h4>
									<ul class="wthree_blog_events_list mt-2">
										<li class="mr-2 text-dark">
											<i class="fa fa-calendar mr-2" aria-hidden="true"></i><?php echo $row['tanggal']; ?></li>
											<li>
												<i class="fa fa-user" aria-hidden="true"></i>
												<a href="#" class="text-dark ml-2">Admin</a>
											</li>
										</ul>
									</div>
								</div>
								
							</div>
						<?php } ?>
					</div>
				</div>
						<!-- //right side -->
					</div>
				</div>
			</div>
			<!-- //blog -->
			<?php
			include('footer.php');
			?>