<?php 
include("../components/header.php");
?>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="../../index.php">Home</a>
		</li>
		<li class="breadcrumb-item active" aria-current="page">List Alumni</li>
	</ol>
</nav>

<div class="about-page py-5">
	<div class="container py-xl-5 py-lg-3">
		<h3 class="title text-capitalize font-weight-light text-dark text-center mb-5">List Alumni</h3>



        
     <table id="tabel-data" class="table table-bordered">
        <thead>
            <tr>
                <th width="10%">Nomor</th>
                <th width="15%">Nama</th>
                <th width="15%">NPM</th>
                 <th width="15%">Tahun Lulus</th>
            </tr>
        </thead>
        <tbody>
            <?php

            //Data mentah yang ditampilkan ke tabel    
             include("../../db_connect.php");
            $show_berita = mysqli_query($connect,"SELECT * FROM  data_pribadi");
              $no=1;
              while($row = mysqli_fetch_array($show_berita)) {
    
            //id = $r['id'];
            ?>

            <tr align='left'>
                <td><?php echo $no;?></td>
                <td><?php echo $row['a2_namalengkap']; ?></td>
                <td><?php echo $row['a1_npm']; ?></td>
                <td><?php echo $row['b2_tahunlulus']; ?></td>
            </tr>
            <?php
                $no++;
            }
            ?>
        </tbody>
    </table>  
  
</div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <!-- <script src="../../assets/js/dataTables.bootstrap.js"></script> -->

    <script type="text/javascript">
        $(function() {
            $('#tabel-data').dataTable();
        });
    </script>



<?php 
// include('../components/footer.php');
?>