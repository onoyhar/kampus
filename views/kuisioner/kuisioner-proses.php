<?php
  date_default_timezone_set("Asia/Jakarta");
  require_once '../../db_connect.php';
  //print($_POST);die;
  if(isset($_POST["submit"])) {
      $id                 = rand(100,99999);
      $a1_npm             = $_POST['a1_npm'];
      $a2_namalengkap     = $_POST['a2_namalengkap'];
      $a3_jeniskelamin    = $_POST['a3_jeniskelamin'];
      $a4_tempatlahir     = $_POST['a4_tempatlahir'];
      $a5_tanggallahir    = $_POST['a5_tanggallahir'];
      $a6_nomorhp         = $_POST['a6_nomorhp'];
      $a7_email           = $_POST['a7_email'];
      $a8_alamatrumah     = $_POST['a8_alamatrumah'];  
      $a9_alamatkantor    = $_POST['a9_alamatkantor'];
      $b1_tahunmasuk      = $_POST['b1_tahunmasuk'];  
      $b2_tahunlulus      = $_POST['b2_tahunlulus'];  
      $b3_kodeprogramstudi= $_POST['b3_kodeprogramstudi'];  
      $b4_setelahlulus    = $_POST['b4_setelahlulus'];  
      $b5_namaperguruan   = $_POST['b5_namaperguruan'];  
      $b6_tahunmasuk      = $_POST['b6_tahunmasuk'];  
      $b7_tahunlulus      = $_POST['b7_tahunlulus'];  
      $b8_jenjangkuliah   = $_POST['b8_jenjangkuliah'];  
      $b9_jurusan         = $_POST['b9_jurusan'];  
      $c1_namatempatkerja = $_POST['c1_namatempatkerja'];  
      $c2_jenisinstansi   = $_POST['c2_jenisinstansi'];  
      $c3_jabatan         = $_POST['c3_jabatan'];  
      $c4_lamabekerja     = $_POST['c4_lamabekerja'];  
      $c5_ratapendapatan  = $_POST['c5_ratapendapatan'];  
      $c6_berhubungan     = $_POST['c6_berhubungan'];  
      $c7_puaskerja       = $_POST['c7_puaskerja'];  
      $c8_pernahbekerja   = $_POST['c8_pernahbekerja'];  
      $c9_bergantikerja   = $_POST['c9_bergantikerja'];  
      $c10_pindahkerja    = $_POST['c10_pindahkerja'];  
      $c11_relevansipendidikankampus = $_POST['c11_relevansipendidikankampus'];
      // $d2_saran                     = $_POST['d2_saran'];
      // $d3_saatbarululus             = $_POST['d3_saatbarululus'];
      $sql = "
        INSERT INTO data_pribadi (
          id,
          a1_npm,
          a2_namalengkap,
          a3_jeniskelamin,
          a4_tempatlahir,
          a5_tanggallahir,
          a6_nomorhp,
          a7_email,
          a8_alamatrumah,
          a9_alamatkantor,
          b1_tahunmasuk,
          b2_tahunlulus,
          b3_kodeprogramstudi,
          b4_setelahlulus,
          b5_namaperguruan,
          b6_tahunmasuk,
          b7_tahunlulus,
          b8_jenjangkuliah,
          b9_jurusan,
          c1_namatempatkerja,
          c2_jenisinstansi,
          c3_jabatan,
          c4_lamabekerja,
          c5_ratapendapatan,
          c6_berhubungan,
          c7_puaskerja,
          c8_pernahbekerja,
          c9_bergantikerja,
          c10_pindahkerja,
          c11_relevansipendidikankampus
          -- d2_saran,
          -- d3_saatbarululus
        )
        VALUES 
        (
          '$id',
          '$a1_npm',
          '$a2_namalengkap',
          '$a3_jeniskelamin',
          '$a4_tempatlahir',
          '$a5_tanggallahir',
          '$a6_nomorhp',
          '$a7_email',
          '$a8_alamatrumah',  
          '$a9_alamatkantor',
          '$b1_tahunmasuk', 
          '$b2_tahunlulus', 
          '$b3_kodeprogramstudi', 
          '$b4_setelahlulus', 
          '$b5_namaperguruan',  
          '$b6_tahunmasuk', 
          '$b7_tahunlulus', 
          '$b8_jenjangkuliah',  
          '$b9_jurusan',  
          '$c1_namatempatkerja',  
          '$c2_jenisinstansi',  
          '$c3_jabatan',  
          '$c4_lamabekerja',  
          '$c5_ratapendapatan', 
          '$c6_berhubungan',  
          '$c7_puaskerja',  
          '$c8_pernahbekerja',  
          '$c9_bergantikerja',  
          '$c10_pindahkerja',
          '$d1_relevansipendidikankampus'
          -- '$d2_saran',
          -- '$d3_saatbarululus'
        )";
       // print_r($sql);die;
    if ($connect-> query($sql) === TRUE ) {
    echo "
    <script type='text/javascript'>
        alert('Kuisioner  Berhasil ditambah');
        window.location = 'datakuisioner.php';
    </script>";
    } else {
    echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
    }
    $connect->close();
    }
?>