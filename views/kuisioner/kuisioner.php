<?php
include("../components/header.php");
?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="../../index.php">Home</a>
		</li>
		<li class="breadcrumb-item active" aria-current="page">Kuisioner</li>
	</ol>
</nav>

<div class="about-page py-5" >
  <div class="container py-xl-5 py-lg-3">
  <form action="kuisioner-proses.php" method="POST" name="form1">
			<h3 class="title text-capitalize font-weight-light text-dark text-center mb-5">Kuisioner
			</h3>
			<div class="col-md-6" style="float: left;">
                <div class="alert alert-primary" role="alert" style="color:#004085; background-color:#cce5ff; border-color:#b8daff;">
                  A. Data Pribadi
                </div>

                <div class="form-group">
                  <h6>A1. NPM</h6>             
                  <input type="text" placeholder="NPM" name="a1_npm" class="form-control required">
                </div>

                <div class="form-group">
                  <h6>A2. Nama Lengkap</h6>             
                  <input type="text" placeholder="Nama" name="a2_namalengkap" class="form-control">
                </div>

                <div class="form-check">
                  <h6>A3. Jenis Kelamin</h6>
                  <label class="form-check-label" for="exampleRadios1">
                  <input class="form-check-input" type="radio" name="a3_jeniskelamin" id="a3_jeniskelamin" value="lakilaki" checked>
                    Laki - Laki
                  </label>
                </div>

                <div class="form-check">
                  <label class="form-check-label" for="exampleRadios1">
                  <input class="form-check-input" type="radio" name="a3_jeniskelamin" id="a3_jeniskelamin" value="perempuan" checked>
                    Perempuan
                  </label>
                </div>

                <div class="form-group">
                  <h6>A4. Tempat Lahir</h6>             
                  <input type="text" placeholder="Tempat Lahir" name="a4_tempatlahir" class="form-control">
                </div>

                <div class="form-group">
                  <h6>A5. Tanggal Lahir</h6>             
                  <input type="text" placeholder="dd/mm/yyyy" name="a5_tanggallahir" class="form-control">
                </div>

                <div class="form-group">
                  <h6>A6. Nomor HP</h6>             
                  <input type="text" placeholder="08xxxxxxxxxx" name="a6_nomorhp" class="form-control">
                </div>

                <div class="form-group">
                  <h6>A7. Email</h6>             
                  <input type="email" placeholder="Enter Email" name="a7_email" class="form-control">
                </div>

                <div class="form-group">
                  <h6>A8. Alamat Rumah</h6>             
                  <textarea class="form-control" name="a8_alamatrumah" ></textarea>
                </div>

                <div class="form-group">
                  <h6>A9. Alamat Kantor</h6>             
                  <textarea class="form-control" name="a9_alamatkantor"></textarea>
                </div>
            </div>
            <div class="col-md-6" style="float: right;">
                <!-- B. Data Pendidikan -->
              <div class="alert alert-primary" role="alert" style="color:#004085; background-color:#cce5ff; border-color:#b8daff;">
                    B. Data Pendidikan
              </div> 


              <div class="form-group">
                  <h6>B1. Tahun Masuk (*ketika tahun masuk)</h6>             
                  <input type="text" placeholder="yyyy" class="form-control" name="b1_tahunmasuk">
              </div>

              <div class="form-group">
                <h6>B2. Tahun Lulus (*ketika tahun lulus)</h6>             
                <input type="text" placeholder="yyyy" class="form-control" name="b2_tahunlulus">
              </div>

              <div class="form-group">
                <h6>B3. kode Program Studi</h6>             
                <!-- <input type="text" placeholder="Name" class="form-control"> -->
                <select class="form-control" name="b3_kodeprogramstudi">
                  <option value="SI-57201">System Informasi</option>
                  <option value="SK-56201">Sistem Komputer</option>
                  <option value="MI-57401">Manajemen Informatika</option>
                  <option value="MI-57401">Manajemen Informatika</option>
                </select>
              </div>

              <div class="form-group">
                <h6>B4. Setelah Lulus apakah anda melanjutkan kuliah lagi ? Jika Tidak, langsung ke C</h6>             
                <!-- <input type="text" placeholder="Name" class="form-control"> -->
                <select class="form-control" name="b4_setelahlulus">
                  <option value="LanjutKuliah">Melanjutkan Kuliah</option>
                  <option value="TidakKuliah">Tidak Melanjutkan Kuliah (Lanjut Ke C)</option>
                </select>
              </div>

              <div class="form-group">
                <h6>B5. Nama Perguruan Tinggi</h6>             
                <input type="text" placeholder="Nama Perguruan Tinggi" class="form-control" name="b5_namaperguruan">
              </div>

              <div class="form-group">
                <h6>B6. Tahun Masuk (*ketik tahun Masuk)</h6>             
                <input type="text" placeholder="yyyy" class="form-control" name="b6_tahunmasuk">
              </div>

              <div class="form-group">
                <h6>B7. Tahun Lulus (*ketik tahun lulus)</h6>             
                <input type="text" placeholder="yyyy" class="form-control" name="b7_tahunlulus">
              </div>

              <div class="form-group">
                <h6>B8. JenjangKuliah</h6>
                <select class="form-control" name="b8_jenjangkuliah">
                  <option value="Strata1">Strata Satu</option>
                  <option value="Strata2">Strata Dua</option>
                  <option value="Strata3">Strata Tiga</option>
                </select>
              </div>

              <div class="form-group">
                <h6>B9. Jurusan</h6>             
                <input type="text" placeholder="Jurusan" class="form-control" name="b9_jurusan">
              </div>
        </div>
    
<hr>


        <div class="col-md-6" style="float: right;">
          <div class="alert alert-primary" role="alert" style="color:#004085; background-color:#cce5ff; border-color:#b8daff;">
                    <!-- D. Relevansi Pendidikan dengan Pekerjaan -->
          </div> 
          <!-- <div class="form-check">
              
              <h6>D1. Apakah pendidikan yang Anda dapat Di STMIk Relevan dengan Pekerjaan Saudara?</h6>
              <input class="form-check-input" type="radio" name="d1_relevansipendidikankampus" id="d1_relevansipendidikankampus" value="Iya" checked>
              <label class="form-check-label" for="exampleRadios1">
                Ya
              </label>
            </div>

            <div class="form-check">
              <input class="form-check-input" type="radio" name="d1_relevansipendidikankampus" id="d1_relevansipendidikankampus" value="Tidak">
              <label class="form-check-label" for="exampleRadios2">
                Tidak
              </label>
            </div>

            <div class="form-group">
              <h6>D2. Dari Pengalaman Anda bekerja, Apa Sarab praktis Anda Untuk Pendidikan DI STMIK Dalam rangak Menigkatkan Kesesuainan Antara Pendidikan dengak Lapangan Pekerjaan?</h6>
              <textarea class="form-control" placeholder="Saran" name="d2_saran"></textarea> 
            </div>

            <div class="form-group">
              <h6>D3. Saat baru Lulus, Apkah Anda Mampu bersaing dengan lulusan Perguruan tinggi lain?</h6>
              <select class="form-control" name="d3_saatbarululus">
                <option value="Sangat Mampu">SangatMampu</option>
                <option value="Mampu">Mampu</option>
                <option value="Kurang Mampu">Kurang Mampu</option>
                <option value="Sangat Kurang Mampu">Sangat Kurang Mampu</option>
              </select>
            </div>    -->  
          <div class="form-check">
            <h6>C7. Apakah Puas Dengan pekerjaan Sekarang?</h6>
            <input class="form-check-input" type="radio" name="c7_puaskerja" id="c7_puaskerja" value="Iya" checked>
             <label class="form-check-label" for="exampleRadios1">
              Ya
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="c7_puaskerja" id="c7_puaskerja" value="Tidak">
            <label class="form-check-label" for="exampleRadios2">
              Tidak
            </label>
          </div>

          <div class="form-check">
            <h6>C8. Sebelumnya, Apakah Anda pernah bekerja Ditempat lain?</h6>
            <input class="form-check-input" type="radio" name="c8_pernahbekerja" id="c8_pernahbekerja" value="Iya" checked>
             <label class="form-check-label" for="exampleRadios1">
              Ya
            </label>
          </div>

          <div class="form-check">
            <input class="form-check-input" type="radio" name="c8_pernahbekerja" id="c8_pernahbekerja" value="Tidak">
            <label class="form-check-label" for="exampleRadios2">
              Tidak
            </label>
          </div>

          <div class="form-group">
            <h6>C9. Berapa Rata-rata Seluruh Pendapatan Anda Sekarang</h6>
            <select class="form-control" name="c9_bergantikerja">
              <option value="< 5.000.000">< 5.000.000</option>
              <option value="5.000.000 sampai 7.000.000">5.000.000 sampai 7.000.000</option>
              <option value="7.000.000< sampai 9.000.000">7.000.000< sampai 9.000.000</option>
              <option value="> 9.000.000">> 9.000.000</option>
            </select>
          </div>

          <div class="form-group">
            <h6>C10. Sudah Berapa kali anda Berganti pekerjaan? (*ktik Jumlah)</h6>
            <input type="text" placeholder="Alasan pindah kerja" class="form-control" name="c10_pindahkerja">
          </div>
          <div class="form-check">
            <h6>C11. Apakah pendidikan yang Anda dapat Di STMIk Relevan dengan Pekerjaan Saudara?</h6>
            <input class="form-check-input" type="radio" name="c11_relevansipendidikankampus" id="d1_relevansipendidikankampus" value="Iya" checked>
            <label class="form-check-label" for="exampleRadios1">
              Ya
            </label>
          </div>
          <div class="form-check">
              <input class="form-check-input" type="radio" name="d1_relevansipendidikankampus" id="d1_relevansipendidikankampus" value="Tidak">
              <label class="form-check-label" for="exampleRadios2">
                Tidak
              </label>
            </div>

        </div>

<hr>

        <!-- C. Data Pekerjaan -->
        <div class="col-md-6" style="float: right;">
          <div class="alert alert-primary" role="alert" style="color:#004085; background-color:#cce5ff; border-color:#b8daff;">
                    C. Data Pekerjaan
          </div> 
        
          <div class="form-group">
            <h6>C1. Nama Tempat Bekerja Sekarang</h6>             
            <input type="text" placeholder="Nama Tempat Bekerja" class="form-control" name="c1_namatempatkerja">
          </div>

          <div class="form-group">
            <h6>C2. Jenis instansi Saat Ini</h6>             
            <input type="text" placeholder="Jenis Instansi" class="form-control" name="c2_jenisinstansi">
          </div>

          <div class="form-group">
            <h6>C3. Jabatan/Posisi Di Pekerjaan Sekarang</h6>
            <input type="text" placeholder="Jabatan" class="form-control" name="c3_jabatan">
          </div>

          <div class="form-group">
            <h6>C4. Bulan dan Tahun Bekerja dipekerjaan Sekarang</h6>
            <input type="text" placeholder="Lama Bekerja" class="form-control" name="c4_lamabekerja">
          </div>

          <div class="form-group">
            <h6>C5. Berapa Rata-rata Seluruh Pendapatan Anda Sekarang</h6>
            <select class="form-control" name="c5_ratapendapatan">
              <option value="< 5.000.000">< 5.000.000</option>
              <option value="5.000.000 sampai 7.000.000">5.000.000 sampai 7.000.000</option>
              <option value="7.000.000< sampai 9.000.000">7.000.000< sampai 9.000.000</option>
              <option value="> 9.000.000">> 9.000.000</option>
            </select>
          </div>

          <div class="form-check">
            <h6>C6. Apakah Pekerjaan Anda Saat Ini Berhubungan dengan Bidang ilmu yang Anda Pelajari</h6>
            <input class="form-check-input" type="radio" name="c6_berhubungan" id="c6_berhubungan" value="Iya" checked>
             <label class="form-check-label" for="exampleRadios1">
              Ya
            </label>
          </div>

          <div class="form-check">
            <input class="form-check-input" type="radio" name="c6_berhubungan" id="c6_berhubungan" value="Tidak">
            <label class="form-check-label" for="exampleRadios2">
              Tidak
            </label>
          </div>

<!--           <div class="form-check">
            <h6>C7. Apakah Puas Dengan pekerjaan Sekarang?</h6>
            <input class="form-check-input" type="radio" name="c7_puaskerja" id="c7_puaskerja" value="Iya" checked>
             <label class="form-check-label" for="exampleRadios1">
              Ya
            </label>
          </div>

          <div class="form-check">
            <input class="form-check-input" type="radio" name="c7_puaskerja" id="c7_puaskerja" value="Tidak">
            <label class="form-check-label" for="exampleRadios2">
              Tidak
            </label>
          </div>

          <div class="form-check">
            <h6>C8. Sebelumnya, Apakah Anda pernah bekerja Ditempat lain?</h6>
            <input class="form-check-input" type="radio" name="c8_pernahbekerja" id="c8_pernahbekerja" value="Iya" checked>
             <label class="form-check-label" for="exampleRadios1">
              Ya
            </label>
          </div>

          <div class="form-check">
            <input class="form-check-input" type="radio" name="c8_pernahbekerja" id="c8_pernahbekerja" value="Tidak">
            <label class="form-check-label" for="exampleRadios2">
              Tidak
            </label>
          </div>

          <div class="form-group">
            <h6>C9. Berapa Rata-rata Seluruh Pendapatan Anda Sekarang</h6>
            <select class="form-control" name="c9_bergantikerja">
              <option value="< 5.000.000">< 5.000.000</option>
              <option value="5.000.000 sampai 7.000.000">5.000.000 sampai 7.000.000</option>
              <option value="7.000.000< sampai 9.000.000">7.000.000< sampai 9.000.000</option>
              <option value="> 9.000.000">> 9.000.000</option>
            </select>
          </div>

          <div class="form-group">
            <h6>C10. Sudah Berapa kali anda Berganti pekerjaan? (*ktik Jumlah)</h6>
            <input type="text" placeholder="Alasan pindah kerja" class="form-control" name="c10_pindahkerja">
          </div>
          <div class="form-check">
            <h6>C11. Apakah pendidikan yang Anda dapat Di STMIk Relevan dengan Pekerjaan Saudara?</h6>
            <input class="form-check-input" type="radio" name="d1_relevansipendidikankampus" id="d1_relevansipendidikankampus" value="Iya" checked>
            <label class="form-check-label" for="exampleRadios1">
              Ya
            </label>
          </div>
          <div class="form-check">
              <input class="form-check-input" type="radio" name="d1_relevansipendidikankampus" id="d1_relevansipendidikankampus" value="Tidak">
              <label class="form-check-label" for="exampleRadios2">
                Tidak
              </label>
            </div>
 -->
         <!--  <div class="form-group">
            <h6>C11. Apakah Anda Masih Ingin Berpindah Bekerja?</h6>
            <input type="text" placeholder="Lama Bekerja" class="form-control" name="c11_mungkinpindahkerja">
          </div> -->
        </div>
         <div class="box-footer"> 
          <button type="submit" name="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
		</div>
	</div>
<?php 
include('../components/footer.php');
?>