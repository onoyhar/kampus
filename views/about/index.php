<?php 
include("../components/header.php");
?>
<!-- breadcrumb -->
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="../../index.php">Home</a>
		</li>
		<li class="breadcrumb-item active" aria-current="page">About Us</li>
	</ol>
</nav>
<!-- breadcrumb -->
	<!-- history -->
	<div class="about-page py-5">
		<div class="container py-xl-5 py-lg-3">
			<h3 class="title text-capitalize font-weight-light text-dark text-center mb-5">about
				<span class="font-weight-bold">us</span>
			</h3>
			<div class="row about-head-wthree">
				<div class="col-lg-6 left-abw3ls">
					<img src="../../assets/images/ap-1.jpg" alt="" class="img-fluid">
				</div>
				<div class="col-lg-6 right-abw3ls mt-lg-0 mt-sm-5 mt-4">
					<h4 class="font-italic border-bottom text-center font-weight-bold pb-3 mb-4">Our History</h4>
					<p style="text-indent: 50px;">Institusi pendidikan tinggi diharapkan banyak berperan baik sebagai sumber ilmu pengetahuan maupun penghasil tenaga kerja terdidik. Proses pembelajaran yang dilaksanakan seharusnya mampu membekali kompetensi lulusan sesuai kebutuhan dunia kerja, sekaligus kompetensi untuk dapat membuka peluang kerja.</p>

					<p style="text-indent: 50px;">STMIK Dharma Negara merupakan sebuah institusi yang menyadari akan pentingnya tanggungjawab dan pengabdian terhadap masyarakat terutama civitas akademikanya. Salah satu tanggung jawabnya adalah menangani pengembangan karir bagi alumni dan pembekalan kompetensi lulusan. Kecepatan dalam memperoleh informasi lowongan pekerjaan dan kompetensi yang dimiliki oleh lulusan diharapkan mampu memperpendek masa tunggu lulusan dalam mendapatkan pekerjaan.</p>
					<p class="mt-3">Untuk itulah dibutuhkan suatu biro layanan yang mampu memenuhi kepentingan dan kebutuhan yang dimaksud. Maka dibentuklah pusat layanan karir bagi mahasiswa dan alumni yaitu STMIK Dharma Negara career centre.
				</div>
			</div>
		</div>
	</div>
	<!-- //history -->

	<!-- mission -->
	<div class="about-page-2 py-5">
		<div class="container-fluid py-xl-5 py-lg-3">
			<div class="row about-head-wthree-2">
				<div class="col-lg-4 left-abw3ls text-lg-left text-center">
					<img src="../../assets/images/ap-2.jpg" alt="" class="img-fluid">
				</div>
				<div class="col-lg-4 right-abw3ls my-lg-0 my-sm-5 my-4">
					<h4 class="font-italic border-bottom text-center font-weight-bold pb-3 mb-4">Our Mission</h4>
					<p>Menjadi pusat pengembangan karir yang mampu menghasilkan lulusan yang unggul dibidangnya dan dapat mengikuti perkembangan ilmu pengetahuan dan teknologi terhadap dunia kerja di era global.</p>
					<p class="mt-3">Sebagai mediator antara mahasiswa dan Alumni STMIK JAKARTA STI&K dengan perusahaan/institusi.</p>
				</div>
				<div class="col-lg-4 left-abw3ls text-lg-left text-center">
					<img src="../../assets/images/ap-3.jpg" alt="" class="img-fluid">
				</div>
			</div>
		</div>
	</div>
	<!-- //mission -->
<?php 
include("../../components/news.php");
include("../components/footer.php");
?>